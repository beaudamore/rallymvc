﻿using System;
using eBayData.Models;
using System.Linq;

namespace eBayData.DAL
{
    public interface IEpicStoryRepository : IDisposable
    {
        IQueryable<EpicStory> GetEpicStories();
        EpicStory GetEpicStoryById(int Id);
        IQueryable<EpicStory> GetEpicStoriesByFeatureId(int FeatureId);
        void InsertEpicStory(EpicStory EpicStory);
        void UpdateEpicStory(EpicStory EpicStory);
        void DeleteEpicStory(int Id);
        void Save();
    }
}