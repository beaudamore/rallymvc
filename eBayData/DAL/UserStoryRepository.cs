﻿using System;
using System.Linq;
using eBayData.Models;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class UserStoryRepository : IUserStoryRepository
    {
        private readonly RallyContext _context;

        public UserStoryRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<UserStory> GetUserStories()
        {
            return _context.UserStories;
        }

        public IQueryable<UserStory> GetUserStoriesByFeatureId(int FeatureId)
        {
            var a = _context.UserStories
                //.Include(x => x.Tasks)
                .Where(x => x.FeatureId == FeatureId);

            return a;
        }     

        public UserStory GetUserStoryById(int Id)
        {
            return _context.UserStories.FirstOrDefault(x => x.Id == Id);
        }

        public void InsertUserStory(UserStory UserStory)
        {
            _context.UserStories.Add(UserStory);
        }

        public void DeleteUserStory(int Id)
        {
            UserStory l = _context.UserStories.Find(Id);
            _context.UserStories.Remove(l);
        }

        public void UpdateUserStory(UserStory UserStory)
        {
            _context.Entry(UserStory).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
