﻿using eBayData.Models;
using System;
using System.Linq;

namespace eBayData.DAL
{
    public interface IUserProfileRepository: IDisposable
    {
        IQueryable<UserProfile> GetUserProfiles();
        UserProfile GetUserProfileById(int Id);
        UserProfile GetUserProfileByUserSID(string UserSID);
        void InsertUserProfile(UserProfile UserProfile);
        void UpdateUserProfile(UserProfile UserProfile);
        void DeleteUserProfile(int Id);
        void Save();
    }
}
