﻿using System;
using System.Linq;
using eBayData.Models;
using System.Data.Entity;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class EpicStoryRepository : IEpicStoryRepository
    {
        private readonly RallyContext _context;

        public EpicStoryRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<EpicStory> GetEpicStories()
        {
            return _context.EpicStories;
        }

        public EpicStory GetEpicStoryById(int Id)
        {
            return _context.EpicStories
                .FirstOrDefault(x => x.Id == Id);
        }

        public IQueryable<EpicStory> GetEpicStoriesByFeatureId(int FeatureId)
        {
            return _context.EpicStories
                .Include(x=>x.UserStories)
                .Where(x => x.FeatureId == FeatureId);
        }

        public void InsertEpicStory(EpicStory EpicStory)
        {
            _context.EpicStories.Add(EpicStory);
        }

        public void DeleteEpicStory(int Id)
        {
            EpicStory l = _context.EpicStories.Find(Id);
            _context.EpicStories.Remove(l);
        }

        public void UpdateEpicStory(EpicStory EpicStory)
        {
            _context.Entry(EpicStory).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
