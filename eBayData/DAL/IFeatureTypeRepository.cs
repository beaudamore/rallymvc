﻿using System;
using eBayData.Models;
using System.Linq;

namespace eBayData.DAL
{
    public interface IFeatureTypeRepository : IDisposable
    {
        IQueryable<FeatureType> GetFeatureTypes();
        FeatureType GetFeatureTypeById(int Id);
        void InsertFeatureType(FeatureType FeatureType);
        void UpdateFeatureType(FeatureType FeatureType);
        void DeleteFeatureType(int Id);
        void Save();
    }
}