﻿using System;
using eBayData.Models;
using System.Linq;

namespace eBayData.DAL
{
    public interface IFeatureRepository : IDisposable
    {
        IQueryable<Feature> GetFeatures();
        Feature GetFeatureById(int Id);
        Feature GetFeatureByIdWithoutStandAloneUserStories(int Id);
        void InsertFeature(Feature Feature);
        void UpdateFeature(Feature Feature);
        void DeleteFeature(int Id);
        void Save();
    }
}