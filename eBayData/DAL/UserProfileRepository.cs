﻿using System;
using System.Linq;
using eBayData.Models;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly RallyContext _context;

        public UserProfileRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<UserProfile> GetUserProfiles()
        {
            return _context.UserProfiles;
        }

        public UserProfile GetUserProfileById(int Id)
        {
            return _context.UserProfiles.FirstOrDefault(x => x.Id == Id);
        }

        public UserProfile GetUserProfileByUserSID(string UserSID)
        {
            return _context.UserProfiles.FirstOrDefault(x => x.SID == UserSID);
        }

        public void InsertUserProfile(UserProfile UserProfile)
        {
            _context.UserProfiles.Add(UserProfile);
        }

        public void DeleteUserProfile(int Id)
        {
            UserProfile l = _context.UserProfiles.Find(Id);
            _context.UserProfiles.Remove(l);
        }

        public void UpdateUserProfile(UserProfile UserProfile)
        {
            _context.Entry(UserProfile).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
