﻿using System;
using System.Linq;

namespace eBayData.DAL
{
    public interface ITaskRepository: IDisposable
    {
        IQueryable<eBayData.Models.Task> GetTasks();
        IQueryable<eBayData.Models.Task> GetTasksByUserStoryId(int UserStoryId);
        eBayData.Models.Task GetTaskById(int Id);
        void InsertTask(eBayData.Models.Task Task);
        void UpdateTask(eBayData.Models.Task Task);
        void DeleteTask(int Id);
        void Save();
    }
}
