﻿//using Microsoft.AspNet.Identity.EntityFramework;
using eBayData.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace eBayData.DAL
{
    public class RallyContext : DbContext
    {
        public RallyContext()
            : base("RallyContext")
        {
            //Database.SetInitializer<CmsContext>(null);// Remove default initializer
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = false;
        }

        //public DbSet<ApplicationUser> Users { get; set; }
        //public DbSet<IdentityRole> Roles { get; set; }

        public DbSet<EpicStory> EpicStories { get; set; }
        public DbSet<UserStory> UserStories { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<FeatureType> FeatureTypes { get; set; }
        public DbSet<eBayData.Models.Task> Tasks { get; set; }
        public DbSet<Error> Errors { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Task>()
                .HasRequired(x => x.UserStory)
                .WithMany()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Feature>()
                .HasOptional(x => x.UserProfile)
                .WithMany()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<EpicStory>()
                .HasOptional(x => x.UserProfile)
                .WithMany()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<UserStory>()
                .HasOptional(x => x.UserProfile)
                .WithMany()
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Task>()
                .HasOptional(x => x.UserProfile)
                .WithMany()
                .WillCascadeOnDelete(true);




        }

    }
}