﻿namespace eBayData.DAL
{
    public class RallyInitializer : System.Data.Entity.DropCreateDatabaseAlways<RallyContext>
    {
        protected override void Seed(RallyContext context)
        {
            // Epic Stories
            //var stories = new List<EpicStory>
            //{
            //    new EpicStory{ Name = "Beau"  },
            //};
            //stories.ForEach(s => context.EpicStories.Add(s));
            //context.SaveChanges();


            // users
            //var users = new List<UserProfile>
            //{
            //    new UserProfile{ EmailAddress="a@a.com", LastName="D'Amore", FirstName="Beau", CellPhoneNumber="888-555-1212", WorkPhoneNumber="888-555-1212", UserProfileGuid = Guid.NewGuid()},          
            //    new UserProfile{ EmailAddress="b@b.com", LastName="Munster", FirstName="Herman", CellPhoneNumber="123-555-7878", WorkPhoneNumber="888-555-1212", UserProfileGuid = Guid.NewGuid()},         
            //    new UserProfile{ EmailAddress="cb@b.com", LastName="Norris", FirstName="Chuck", CellPhoneNumber="456-555-7878", WorkPhoneNumber="888-555-1212", UserProfileGuid = Guid.NewGuid()},        
            //    new UserProfile{ EmailAddress="fb@b.com", LastName="Skywalker", FirstName="Luke", CellPhoneNumber="789-555-7878", WorkPhoneNumber="888-555-1212", UserProfileGuid = Guid.NewGuid()},        
            //    new UserProfile{ EmailAddress="fjjb@b.com", LastName="Flintstone", FirstName="Fred", CellPhoneNumber="456-555-4444", WorkPhoneNumber="888-555-1212", UserProfileGuid = Guid.NewGuid()}          
            //};
            //users.ForEach(s => context.UserProfiles.Add(s));
            //context.SaveChanges();

            //// freeform groups
            //var freeFormGroups = new List<FreeFormGroup>
            //{
            //    new FreeFormGroup
            //    {
            //        Title = "Property"     
            //    }
            //    ,new FreeFormGroup
            //    {
            //        Title = "Executive"     
            //    }
            //    ,new FreeFormGroup
            //    {
            //        Title = "ResearchArticle"     
            //    }
            //    ,new FreeFormGroup
            //    {
            //        Title = "BayOption"     
            //    }
            //};
            //freeFormGroups.ForEach(s => context.FreeFormGroups.Add(s));

            //// for testing
            //try
            //{
            //    context.SaveChanges();
            //}
            //catch(Exception ex)
            //{
            //    var test = ex.Message;
            //    // just checkin'
            //}

            //// freeform template
            //// -Make some Templates
            //var freeFormStringTemplates = new List<FreeFormTemplate>(){
            //    new FreeFormTemplate{
            //        FreeFormTemplateId = Guid.NewGuid()
            //        , ValueType = Enums.FreeFormValueType.Number
            //        , Label = "Square Footage"
            //        , Suffix = "sq/ft"
            //    },
            //    new FreeFormTemplate{
            //        FreeFormTemplateId = Guid.NewGuid()
            //        , ValueType = Enums.FreeFormValueType.String
            //        , Label = "Property Type"
            //         //, Suffix = null
            //    }
            //};
            //freeFormStringTemplates.ForEach(s => context.FreeFormTemplates.Add(s));

            //// -some sample fields
            //var freeformFields = new List<FreeFormField>
            //{
            //    new FreeFormField
            //    {
            //        FreeFormTemplate = freeFormStringTemplates.Where(x=>x.Label=="Square Footage").First()
            //        , FreeFormGroup = freeFormGroups.Where(x => x.Title == "Property").First()
            //        , NumberValue = 5000 
            //    }
            //    ,new FreeFormField
            //    {
            //        FreeFormTemplate = freeFormStringTemplates.Where(x=>x.Label=="Property Type").First()
            //        , FreeFormGroup = freeFormGroups.Where(x => x.Title == "Property").First()
            //        , StringValue = "Property Type I"     
            //      }
            //};
            //freeformFields.ForEach(s => context.FreeFormFields.Add(s));
            //context.SaveChanges();

            //// image contents
            //// get image by local path
            //var contents = new List<Content>
            //{
            //    new Content{ ContentBytes = File.ReadAllBytes(MapPath("~/SeedFiles/test.jpg")), AddedDate = DateTime.Now, AddedByUserId = 1}, 
            //    new Content{ ContentBytes = File.ReadAllBytes(MapPath("~/SeedFiles/test2.jpg")), AddedDate = DateTime.Now, AddedByUserId = 1}                              
            //};

            //// nay types
            //var bayTypes = new List<BayType>
            //{
            //    new BayType
            //    {
            //        BayTypeGuid =  Guid.NewGuid(),
            //        Name = "test"
            //    }
            //};

            //// bay options
            //var bayOptions = new List<BayOption>
            //{
            //    new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "123"
            //        , BaySize = 3000
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "Test Bay description"
            //        , Excerpt = "Excerpt Text for testing"
            //        , IsFeatured = true
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties
            //        , OccupancyState = Enums.OccupancyState.Available
            //        , OccupancyStateOverride = true
            //        , OccupancyStateOverrideValue = Enums.OccupancyState.Occupied
            //    },
            //     new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "456"
            //        , BaySize = 7000
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "another testing description"
            //        , Excerpt = "for testing"
            //        , IsFeatured = false
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate 
            //        , OccupancyState = Enums.OccupancyState.Occupied      
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "overridden description"
            //    }
            //}; 
            //var bayOptions2 = new List<BayOption>
            //{
            //    new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "678"
            //        , BaySize = 2500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = " Bay description"
            //        , Excerpt = "Excerpt Text "
            //        , IsFeatured = true
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk
            //        , OccupancyState = Enums.OccupancyState.Available
            //        , OccupancyStateOverride = true
            //        , OccupancyStateOverrideValue = Enums.OccupancyState.Occupied
            //    },
            //     new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "901"
            //        , BaySize = 4500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "testing description"
            //        , Excerpt = "for testing"
            //        , IsFeatured = false
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate 
            //        , OccupancyState = Enums.OccupancyState.Occupied      
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "overridden description"
            //    }
            //};
            //var bayOptions3 = new List<BayOption>
            //{
            //    new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "678123"
            //        , BaySize = 5500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "Bay description3"
            //        , Excerpt = "Excerpt Text3"
            //        , IsFeatured = true
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk
            //        , OccupancyState = Enums.OccupancyState.Available
            //        , OccupancyStateOverride = true
            //        , OccupancyStateOverrideValue = Enums.OccupancyState.Occupied
            //    },
            //     new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "90133"
            //        , BaySize = 34500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "testing description33"
            //        , Excerpt = "for testing33"
            //        , IsFeatured = false
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate 
            //        , OccupancyState = Enums.OccupancyState.Occupied      
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "overridden description33"
            //    }
            //};
            //var bayOptions4 = new List<BayOption>
            //{
            //    new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "4444"
            //        , BaySize = 4400
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "Bay4 description4"
            //        , Excerpt = "Excerpt4 Text4"
            //        , IsFeatured = true
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk
            //        , OccupancyState = Enums.OccupancyState.Available
            //        , OccupancyStateOverride = true
            //        , OccupancyStateOverrideValue = Enums.OccupancyState.Occupied
            //    },
            //     new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "90144"
            //        , BaySize = 34500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "testing description44"
            //        , Excerpt = "for testing44"
            //        , IsFeatured = false
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate 
            //        , OccupancyState = Enums.OccupancyState.Occupied      
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "overridden description44"
            //    }
            //};
            //var bayOptions5 = new List<BayOption>
            //{
            //    new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "55555"
            //        , BaySize = 5500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "Bay5 description555"
            //        , Excerpt = "Excerp55 Text5"
            //        , IsFeatured = true
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk
            //        , OccupancyState = Enums.OccupancyState.Available
            //        , OccupancyStateOverride = true
            //        , OccupancyStateOverrideValue = Enums.OccupancyState.Occupied
            //    },
            //     new BayOption{ 
            //        BayGUID = Guid.NewGuid()
            //        , BayNumber = "90555"
            //        , BaySize = 34500
            //        , BayType = bayTypes.FirstOrDefault() 
            //        , Description = "testing description555"
            //        , Excerpt = "for test555"
            //        , IsFeatured = false
            //        , ProjectGUID = Guid.NewGuid()
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate 
            //        , OccupancyState = Enums.OccupancyState.Occupied      
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "overridden descriptio555"
            //    }
            //};

            //// Tags
            //var Tags = new List<Tag>
            //{
            //   new Tag
            //   {
            //       TagName = "Commercial"
            //       , CreatedDate = DateTime.Now
            //   },
            //   new Tag
            //   {
            //       TagName = "First Floor"
            //       , CreatedDate = DateTime.Now
            //   },
            //   new Tag
            //   {
            //       TagName = "Street Facing"
            //       , CreatedDate = DateTime.Now
            //   }
            //};

            //// Location              
            //DbGeography location = DbGeography.FromText("POINT(-80.125422 26.365712)");
            //DbGeography location2 = DbGeography.FromText("POINT(-80.090573 26.126350)");
            //DbGeography location3 = DbGeography.FromText("POINT(-81.490573 26.156350)");
            //DbGeography location4 = DbGeography.FromText("POINT(-81.190573 26.026350)");
            //DbGeography location5 = DbGeography.FromText("POINT(-81.140573 26.186350)");

            //// properties     
            ////System.Data.Entity.DbSet<Property> properties = context.Properties;
            //List<Property> PropList = new List<Property>{
            //    new Property
            //    { 
            //        ProjectGuid = Guid.NewGuid(),
            //        Name = "Woolbright Corporate"
            //        , Address = "2240 NW 19th St"
            //        , Address2 = "Suite 801"
            //        , BayOptions = bayOptions.ToList()
            //        , City = "Boca Raton"
            //        , Description = "Test Property"
            //        , GrossLeasableArea = 10000
            //        , Location = location
            //        , Market = markets.First()
            //        , MaxContiguous = 1000
            //        , MinDivisible = 500
            //        , FreeFormFields = freeformFields
            //        , CorporateContact = users.ToArray()[0]
            //        , LeasingAgent = users.ToArray()[1]
            //        , PropertyContent = contents.ToArray()[0]
            //        , SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate | Enums.SiteVisibilityFlags.Properties
            //        , State = "FL"
            //        , SquareFootage = 10000
            //        , Tags = Tags
            //        , WebsiteUrl = "http://www.woolbright.com"
            //        , Zip = "33431"

            //        // overrides
            //        , DescriptionOverride = true
            //        , DescriptionOverrideValue = "a new description override"
            //    },      
            //    new Property
            //    {
            //         ProjectGuid = Guid.NewGuid(),
            //         Name = "Publix Boynton Beach",
            //         Address = "133 Congress Ave",
            //         Address2 = "",
            //         BayOptions = bayOptions2.ToList(),
            //         City = "Boynton Beach",
            //         Description = "Test Property",
            //         GrossLeasableArea = 8000,
            //         Location = location2,
            //         Market = markets.First(),
            //         MaxContiguous = 8000,
            //         MinDivisible = 1000,
            //         //FreeFormFields = freeformFields,
            //         CorporateContact = users.ToArray()[1],
            //         LeasingAgent = users.ToArray()[0],
            //         PropertyContent = contents.ToArray()[1],
            //         SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk,
            //         State = "FL",
            //         SquareFootage = 10000,
            //         //Tags = Tags,
            //         WebsiteUrl = "www.publix.com",
            //         Zip = "33436"
            //     },
            //    new Property
            //    {
            //         ProjectGuid = Guid.NewGuid(),
            //         Name = "Testing",
            //         Address = "13223 Congress Ave",
            //         Address2 = "",
            //         BayOptions = bayOptions3.ToList(),
            //         City = "Boynton Beach",
            //         Description = "Test Property",
            //         GrossLeasableArea = 30000,
            //         Location = location3,
            //         Market = markets[2],//.First(),
            //         MaxContiguous = 30000,
            //         MinDivisible = 15000,
            //         //FreeFormFields = freeformFields,
            //         CorporateContact = users.ToArray()[2],
            //         LeasingAgent = users.ToArray()[3],
            //         PropertyContent = contents.ToArray()[1],//.Last(),
            //         SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Kiosk,
            //         State = "FL",
            //         SquareFootage = 10000,
            //         //Tags = Tags,
            //         WebsiteUrl = "www.whoknows.com",
            //         Zip = "33437"
            //     },
            //    new Property
            //    {
            //         ProjectGuid = Guid.NewGuid(),
            //         Name = "Testing44",
            //         Address = "4444 Congress Ave",
            //         Address2 = "",
            //         BayOptions = bayOptions4.ToList(),
            //         City = "Boynton Beach",
            //         Description = "Test4 Property4",
            //         GrossLeasableArea = 40000,
            //         Location = location4,
            //         Market = markets[3],//.First(),
            //         MaxContiguous = 40000,
            //         MinDivisible = 4000,
            //         //FreeFormFields = freeformFields,
            //         CorporateContact = users.ToArray()[2],
            //         LeasingAgent = users.ToArray()[4],
            //         PropertyContent = contents.ToArray()[1],//.Last(),
            //         SiteVisibilityFlags = Enums.SiteVisibilityFlags.Corporate,
            //         State = "FL",
            //         SquareFootage = 40000,
            //         //Tags = Tags,
            //         WebsiteUrl = "www.whoknows.com",
            //         Zip = "33437"
            //     },
            //    new Property
            //    {
            //         ProjectGuid = Guid.NewGuid(),
            //         Name = "Testing555",
            //         Address = "5555 Congress Ave",
            //         Address2 = "",
            //         BayOptions = bayOptions5.ToList(),
            //         City = "Boynton Beach",
            //         Description = "555 Property5",
            //         GrossLeasableArea = 40000,
            //         Location = location5,
            //         Market = markets.First(),
            //         MaxContiguous = 50000,
            //         MinDivisible = 5000,
            //         //FreeFormFields = freeformFields,
            //         CorporateContact = users.ToArray()[2],
            //        LeasingAgent = users.ToArray()[3],
            //        PropertyContent = contents.ToArray()[0],
            //         SiteVisibilityFlags = Enums.SiteVisibilityFlags.Properties | Enums.SiteVisibilityFlags.Corporate,
            //         State = "FL",
            //         SquareFootage = 50000,
            //         //Tags = Tags,
            //         WebsiteUrl = "www.w55ws.com",
            //         Zip = "33435"
            //     }
            //};
            ////context.Properties.Add(p);
            ////context.SaveChanges();
            ////context.Properties.Add(pp);
            //PropList.ForEach(s => context.Properties.Add(s));
            //context.SaveChanges();

            //// === excutives ===
            //ExecutiveSection execSec = new ExecutiveSection();
            //execSec.SectionName = "Executive Team";
            //execSec.SortOrder = 1;
            //context.ExecutiveSections.Add(execSec);

            //ExecutiveSection execSec2 = new ExecutiveSection();
            //execSec2.SectionName = "Acquisitions";
            //execSec2.SortOrder = 2;
            //context.ExecutiveSections.Add(execSec2);

            //ExecutiveSection execSec3 = new ExecutiveSection();
            //execSec3.SectionName = "Leasing";
            //execSec3.SortOrder = 3;
            //context.ExecutiveSections.Add(execSec3);

            //ExecutiveSection execSec4 = new ExecutiveSection();
            //execSec4.SectionName = "Operations";
            //execSec4.SortOrder = 4;
            //context.ExecutiveSections.Add(execSec4);

            //ExecutiveSection execSec5 = new ExecutiveSection();
            //execSec5.SectionName = "Legal";
            //execSec5.SortOrder = 5;
            //context.ExecutiveSections.Add(execSec5);

            //Executive exec = new Executive();
            //exec.ExecutiveBlurb = "test blurb";
            //exec.ExecutiveTitle = "President";
            //exec.ProfilePictureContent = new Content { ContentBytes = File.ReadAllBytes("c:\\exec.jpg"), AddedDate = DateTime.Now, AddedByUserId = 1 };
            //exec.ProfileSidePictureContent = new Content { ContentBytes = File.ReadAllBytes("c:\\exec2.jpg"), AddedDate = DateTime.Now, AddedByUserId = 1 };
            //exec.UserId = 1;
            //exec.User = users.Where(x => x.UserId == 1).First();
            //exec.ExecutiveSections = context.ExecutiveSections.Where(x => x.SectionName == "Executive Team" && x.SectionName == "Acquisitions").ToList();
            //context.Executives.Add(exec);

            //User u = context.Users.Where(x => x.UserId == 1).First();
            //u.ExecutiveId = exec.ExecutiveId;

            //Executive exec2 = new Executive();
            //exec2.ExecutiveBlurb = "Vp blurb";
            //exec2.ExecutiveTitle = "Vice President";
            //exec2.ProfilePictureContent = new Content { ContentBytes = File.ReadAllBytes("c:\\exec3.jpg"), AddedDate = DateTime.Now, AddedByUserId = 1 };
            //exec2.ProfileSidePictureContent = new Content { ContentBytes = File.ReadAllBytes("c:\\exec4.jpg"), AddedDate = DateTime.Now, AddedByUserId = 1 };
            //exec2.UserId = 2;
            //exec2.ExecutiveSections = context.ExecutiveSections.Where(x => x.SectionName == "Executive Team").ToList();
            //context.Executives.Add(exec2);

            // SAVE
            //context.SaveChanges();            

        }
        //private string MapPath(string seedFile)
        //{
        //    if (HttpContext.Current != null)
        //        return HostingEnvironment.MapPath(seedFile);

        //    var absolutePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
        //    var directoryName = Path.GetDirectoryName(absolutePath);
        //    var path = Path.Combine(directoryName, ".." + seedFile.TrimStart('~').Replace('/', '\\'));

        //    return path;
        //}
    }
}