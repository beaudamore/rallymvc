﻿using System;
using System.Linq;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class TaskRepository : ITaskRepository
    {
        private readonly RallyContext _context;

        public TaskRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<eBayData.Models.Task> GetTasks()
        {
            return _context.Tasks;
        }

        public IQueryable<eBayData.Models.Task> GetTasksByUserStoryId(int UserStoryId)
        {
            return _context.Tasks
                .Where(x => x.UserStoryId == UserStoryId);
        }     

        public eBayData.Models.Task GetTaskById(int Id)
        {
            return _context.Tasks.FirstOrDefault(x => x.Id == Id);
        }

        public void InsertTask(eBayData.Models.Task Task)
        {
            _context.Tasks.Add(Task);
        }

        public void DeleteTask(int Id)
        {
            eBayData.Models.Task l = _context.Tasks.Find(Id);
            _context.Tasks.Remove(l);
        }

        public void UpdateTask(eBayData.Models.Task Task)
        {
            _context.Entry(Task).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
