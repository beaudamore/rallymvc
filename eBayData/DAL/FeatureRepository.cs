﻿using System;
using System.Linq;
using eBayData.Models;
using System.Data.Entity;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class FeatureRepository : IFeatureRepository
    {
        private readonly RallyContext _context;

        public FeatureRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<Feature> GetFeatures()
        {
            return _context.Features.Include(x=>x.UserStories);
        }

        public Feature GetFeatureById(int Id)
        {
            return _context.Features
                .Include(x=>x.EpicStories)
                .Include(y=>y.UserStories)
                .FirstOrDefault(x => x.Id == Id);
        }
        public Feature GetFeatureByIdWithoutStandAloneUserStories(int Id)
        {
            // TODO: this doesn't work 
            return _context.Features
                .FirstOrDefault(x => x.UserStories.Any(us => us.EpicStoryId.HasValue && x.Id == Id));
        }

        public void InsertFeature(Feature Feature)
        {
            _context.Features.Add(Feature);
        }

        public void DeleteFeature(int Id)
        {
            Feature l = _context.Features.Find(Id);
            _context.Features.Remove(l);
        }

        public void UpdateFeature(Feature Feature)
        {
            _context.Entry(Feature).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
