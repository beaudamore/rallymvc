﻿using eBayData.Models;
using System;
using System.Linq;

namespace eBayData.DAL
{
    public interface IUserStoryRepository: IDisposable
    {
        IQueryable<UserStory> GetUserStories();
        IQueryable<UserStory> GetUserStoriesByFeatureId(int FeatureId);
        UserStory GetUserStoryById(int Id);
        void InsertUserStory(UserStory UserStory);
        void UpdateUserStory(UserStory UserStory);
        void DeleteUserStory(int Id);
        void Save();
    }
}
