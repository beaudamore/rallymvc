﻿using System;
using System.Linq;

namespace eBayData.DAL
{
    public interface IErrorRepository: IDisposable
    {
        IQueryable<eBayData.Models.Error> GetErrors();
        eBayData.Models.Error GetErrorById(int Id);
        void InsertError(eBayData.Models.Error Error);
        void UpdateError(eBayData.Models.Error Error);
        void DeleteError(int Id);
        void Save();
    }
}
