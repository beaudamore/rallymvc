﻿using System;
using System.Linq;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class ErrorRepository : IErrorRepository
    {
        private readonly RallyContext _context;

        public ErrorRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<eBayData.Models.Error> GetErrors()
        {
            return _context.Errors;
        }
        
        public eBayData.Models.Error GetErrorById(int Id)
        {
            return _context.Errors.FirstOrDefault(x => x.Id == Id);
        }

        public void InsertError(eBayData.Models.Error Error)
        {
            _context.Errors.Add(Error);
        }

        public void DeleteError(int Id)
        {
            eBayData.Models.Error l = _context.Errors.Find(Id);
            _context.Errors.Remove(l);
        }

        public void UpdateError(eBayData.Models.Error Error)
        {
            _context.Entry(Error).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
