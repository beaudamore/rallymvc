﻿using System;
using System.Linq;
using eBayData.Models;
using System.Data.Entity;
using eBayData.DAL;

namespace RallyMVC.DAL
{
    public class FeatureTypeRepository : IFeatureTypeRepository
    {
        private readonly RallyContext _context;

        public FeatureTypeRepository(RallyContext context)
        {
            this._context = context;
        }

        public IQueryable<FeatureType> GetFeatureTypes()
        {
            return _context.FeatureTypes.Include(x => x.Features);
        }

        public FeatureType GetFeatureTypeById(int Id)
        {
            return _context.FeatureTypes.FirstOrDefault(x => x.Id == Id);
        }
      
        public void InsertFeatureType(FeatureType FeatureType)
        {
            _context.FeatureTypes.Add(FeatureType);
        }

        public void DeleteFeatureType(int Id)
        {
            FeatureType l = _context.FeatureTypes.Find(Id);
            _context.FeatureTypes.Remove(l);
        }

        public void UpdateFeatureType(FeatureType FeatureType)
        {
            _context.Entry(FeatureType).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
               
    }
    
}
