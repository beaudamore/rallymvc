﻿using eBayData.Classes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayData.Models
{
    public class Task
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int SortOrderId { get; set; }

        [DisplayName("Name"), Required]
        public string Name { get; set; }

        [DisplayName("Description"), Required]
        public string Description { get; set; }

        [DisplayName("Schedule State"), Required]
        public Enums.ScheduleStateTask? ScheduleStateTask { get; set; }

        [DisplayName("Notes")]
        public string Notes { get; set; }

        [DisplayName("Estimated Time")]
        public decimal EstimatedTime { get; set; }

        [DisplayName("Expedite")]
        public bool? IsExpedited { get; set; }

        // --- FOREIGN KEYS ---
        public int UserStoryId { get; set; }
        [ForeignKey("UserStoryId")]
        public virtual UserStory UserStory { get; set; }

        public int? UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }


    }
}