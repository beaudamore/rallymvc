﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayData.Models
{
    public class UserProfile
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Full Name"), Required]
        public string FullName { get; set; }

        [DisplayName("SID"), Required]
        public string SID { get; set; }

        [DisplayName("Rally Username"), Required]
        public string RallyUsername { get; set; }

        [DisplayName("Rally Password"), Required]
        public string RallyPassword { get; set; }

        [DisplayName("Rally Reference"), Required]
        public string RallyReference { get; set; }

        //public Enums.UserPermissionFlags UserPermissionFlags { get; set; }
        [Required]
        public bool IsActive { get; set; }

        // related user/owner
        [InverseProperty("UserProfile")]
        public virtual ICollection<Feature> Features { get; set; }

        // related user/owner
        [InverseProperty("UserProfile")]
        public virtual ICollection<EpicStory> EpicStories { get; set; }

        // related user/owner
        [InverseProperty("UserProfile")]
        public virtual ICollection<UserStory> UserStories { get; set; }

        // related user/owner
        [InverseProperty("UserProfile")]
        public virtual ICollection<Task> Tasks { get; set; }



    }
}