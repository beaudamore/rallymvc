﻿using eBayData.Classes;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayData.Models
{
    public class UserStory
    {
        [Key]
        public int Id { get; set; }
        public int SortOrderId { get; set; }

        public string RallyProjectId { get; set; }
        public string RallyProjectName { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public string Notes { get; set; }

        [DisplayName("Estimated Time")]
        public decimal EstimatedTime { get; set; }

        [DisplayName("Schedule State"), Required]
        public Enums.ScheduleStateTask ScheduleStateTask { get; set; }

        [DisplayName("Expedite")]
        public bool IsExpedited { get; set; }
        
        // -- Foreign Keys --
        public int? EpicStoryId { get; set; }
        [ForeignKey("EpicStoryId")]
        public virtual EpicStory EpicStory { get; set; }

        public int FeatureId { get; set; }
        [ForeignKey("FeatureId")]
        public virtual Feature Feature { get; set; }

        [InverseProperty("UserStory")]
        public virtual ICollection<Task> Tasks { get; set; }

        public int? UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }

    }
}
