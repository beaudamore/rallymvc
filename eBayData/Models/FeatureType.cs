﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eBayData.Models
{
    public class FeatureType
    {

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        // -- InverseProperties/Foreign Keys --
        [InverseProperty("FeatureType")]
        public virtual ICollection<Feature> Features { get; set; }


    }
}
