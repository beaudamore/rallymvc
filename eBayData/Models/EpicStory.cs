﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eBayData.Models
{
    public class EpicStory
    {
        [Key]
        public int Id { get; set; }

        public int SortOrderId { get; set; }

        public string RallyProjectId { get; set; }
        public string RallyProjectName { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public string Notes { get; set; }

        [DisplayName("Estimated Time")]
        public decimal EstimatedTime { get; set; }

        public int FeatureId { get; set; }
        [ForeignKey("FeatureId")]
        public virtual Feature Feature { get; set; }

        [InverseProperty("EpicStory")]
        public virtual ICollection<UserStory> UserStories { get; set; }

        public int? UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }

    }
}
