﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace eBayData.Models
{
    public class Feature
    {
        [Key]
        public int Id { get; set; }

        public string RallyProjectId { get; set; }
        public string RallyProjectName { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        // Preliminary Estimate
        public string PreliminaryEstimateName { get; set; }
        public string PreliminaryEstimateId { get; set; }

        //[DisplayName("Schedule State"), Required]
        //public Enums.ScheduleStateFeature ScheduleStateFeature { get; set; }
        
        // -- InverseProperties/Foreign Keys --
        [InverseProperty("Feature")]
        public virtual ICollection<UserStory> UserStories { get; set; }

        [InverseProperty("Feature")]
        public virtual ICollection<EpicStory> EpicStories { get; set; }

        // one FeatureType per feature
        [Required]
        public int FeatureTypeId { get; set; }
        [ForeignKey("FeatureTypeId")]
        public virtual FeatureType FeatureType { get; set; }

        // One user Profile per feature as owner
        public int? UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }
        
    }
}
