﻿using System;
using System.ComponentModel.DataAnnotations;

namespace eBayData.Models
{
    public class Error
    {
        [Key]
        public int Id { get; set; }

        public string Message { get; set; }

        public string InnerException { get; set; }

        public string StackTrace { get; set; }

        public DateTime ErrorDate { get; set; }

        public string User { get; set; }

    }
}
