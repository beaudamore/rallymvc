namespace eBayData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnkey2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FeatureType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.Feature", "FeatureTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Feature", "FeatureTypeId");
            Sql(@"INSERT INTO FeatureType (name) VALUES ('New Feature')");
            Sql(@"INSERT INTO FeatureType (name) VALUES ('Update Existing Feature')");
            AddForeignKey("dbo.Feature", "FeatureTypeId", "dbo.FeatureType", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Feature", "FeatureTypeId", "dbo.FeatureType");
            DropIndex("dbo.Feature", new[] { "FeatureTypeId" });
            AlterColumn("dbo.Feature", "FeatureTypeId", c => c.Int());
            DropTable("dbo.FeatureType");
        }
    }
}
