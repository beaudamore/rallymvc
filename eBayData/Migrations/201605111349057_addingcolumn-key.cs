namespace eBayData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnkey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Feature", "FeatureTypeId", c => c.Int());

            Sql(@"UPDATE dbo.Feature SET FeatureTypeId = 1
              where FeatureTypeId IS NULL");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Feature", "FeatureTypeId");
        }
    }
}
