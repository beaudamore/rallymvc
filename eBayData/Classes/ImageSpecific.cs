﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using eBayData.Models;

//using System.Configuration;

namespace eBayData.Classes
{
    public static class ImageSpecific
    {
        //public static ImageFormat GetImageFormat(string extension)
        //{
        //    ImageFormat result = null;
        //    PropertyInfo prop = typeof(ImageFormat)
        //        .GetProperties()
        //        .FirstOrDefault(p => p.Name.Equals(extension, StringComparison.InvariantCultureIgnoreCase));
        //    if (prop != null)
        //    {
        //        result = prop.GetValue(prop) as ImageFormat;
        //    }
        //    return result;
        //}
        //public static Content ValidateImageContent(HttpPostedFileBase image, out string errMsg)
        //{
        //    errMsg = string.Empty;
        //    bool goodFormat = false;
        //    try
        //    {
        //        // see if it IS an image first...
        //        using (var img = Image.FromStream(image.InputStream))
        //        {
        //            // Use 'AcceptedImageTypes' key in web.config to validate uploaded image type
        //            // ==========================================================================
        //            List<ImageFormat> imageFormats = new List<ImageFormat>();

        //            // get accepted extensions from web/config
        //            List<string> AcceptedImageTypes = System.Configuration.ConfigurationManager.AppSettings["AcceptedImageTypes"]
        //                                                    .Split('|')
        //                                                    .ToList();
        //            // convert extension strings to ImageFormats
        //            foreach (string s in AcceptedImageTypes)
        //            {
        //                ImageFormat format = ImageSpecific.GetImageFormat(s);
        //                imageFormats.Add(format);
        //            }
        //            // compare the image to the list of accepted formats
        //            foreach (ImageFormat i in imageFormats)
        //            {
        //                if (img.RawFormat.Equals(i))
        //                {
        //                    goodFormat = true;
        //                    break;
        //                }
        //            }
        //            // Let the user know it's not a type in the list of accepted extensions
        //            if (!goodFormat)
        //            {
        //                errMsg = "Wrong Image Type. Please use an image one with the following extenstions:<br /><b>.jpeg, .gif, .png, or .bmp</b>";
        //                //return View(property);
        //            }
        //        }
        //    }
        //    catch // image from stream failed, not an image file type
        //    {
        //        errMsg = "Wrong File type. Please use an image one with the following extenstions:<br /><b>.jpeg, .gif, .png, or .bmp</b>";
        //        //return View(property);
        //    }

        //    // check size from app key
        //    int MaxImageUploadSizeInMb = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxImageUploadSizeInMB"]);
        //    if (image.ContentLength > MaxImageUploadSizeInMb * 1024 * 1024) // 5 MB
        //    {
        //        errMsg = "Image over 5mb. Please upload a smaller image.";
        //        //return View(property);
        //    }
        //    /*
        //    * All good, read image into byte[]
        //    */
        //    MemoryStream target = new MemoryStream();
        //    image.InputStream.Position = 0; // this line was KEY in solving the 0 bytes problem
        //    image.InputStream.CopyTo(target);
        //    byte[] imageBytes = target.ToArray();
        //    Content propertyImage = new Content
        //    {
        //        AddedByUserId = 1 // TODO: tweak this when we go to AD for user list
        //         ,
        //        AddedDate = DateTime.Now
        //         ,
        //        ContentBytes = imageBytes
        //         ,
        //        ContentType = (int)Enums.ContentTypes.Image
        //    };

        //    // return the image
        //    return propertyImage;

        //}

        //public static MvcHtmlString ActionImage(this HtmlHelper html, string linkText, string action, string controller, string imagePath, string alt, object routeValues)
        //{
        //    var url = new UrlHelper(html.ViewContext.RequestContext);

        //    // build the <img> tag
        //    var imgBuilder = new TagBuilder("img");
        //    imgBuilder.MergeAttribute("src", url.Content(imagePath));
        //    imgBuilder.MergeAttribute("alt", alt);
        //    string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

        //    // build the <a> tag
        //    var anchorBuilder = new TagBuilder("a");
        //    //anchorBuilder.SetInnerText(LinkText);
        //    anchorBuilder.MergeAttribute("href", url.Action(action, controller, routeValues));
        //    anchorBuilder.InnerHtml = "<div style=\"display: inline;\">" + imgHtml + linkText + "</div>"; // include the <img> tag inside
        //    //anchorBuilder.InnerHtml = imgHtml  + LinkText ; // include the <img> tag inside
        //    //anchorBuilder.InnerHtml = "<table><tr><td align='center'>" + imgHtml + "</td></tr><tr><td align='center'>" + LinkText + "</td></tr></table>"; // include the <img> tag inside
        //    string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

        //    return MvcHtmlString.Create(anchorHtml);
        //}
    }
}