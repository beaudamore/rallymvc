﻿using System;

namespace eBayData.Classes
{
    public static class Enums
    {
        public enum ScheduleStateFeature
        {
            Discovering,
            Developing,
            Measuring,
            Done
        }
        public enum ScheduleStateStory
        {
            Defined,
            InProgress,
            Completed,
            Accepted
        }
        public enum ScheduleStateTask
        {
            Defined,
            InProgress,
            Completed
        }

        public enum ContentTypes
        {
            Image,
            Video,
            Document
        }

        public enum FreeFormValueType
        {
            String,
            Number,
            Location,
            Content,
            Tag
        }

        [Flags] // Must start at '1' not '0' for Flags-Enums
        public enum UserPermissionFlags : short
        {
            None = 1,
            Add = 2,
            Edit = 4,
            Delete = 8,
            Full = 16
        }
        private static UserPermissionFlags DetermineUserPermissionFlags(string[] userPermissionFlags)
        {
            var userPermissionFlagsTotalValue = 0;
            if (userPermissionFlags != null)
            {
                foreach (var upf in userPermissionFlags)
                {
                    UserPermissionFlags flagTester;
                    if (Enum.TryParse(upf, out flagTester))
                    {
                        // sum up values to get total and then convert to enum after
                        userPermissionFlagsTotalValue += (int)flagTester;
                    }
                }
            }
            // be sure to account for nothing checked ('1' is 'None', not '0')
            if (userPermissionFlagsTotalValue == 0)
                userPermissionFlagsTotalValue = 1;
            // convert siteVisibilityTotalValue to Enum
            return (UserPermissionFlags)userPermissionFlagsTotalValue;
        }

    }
}