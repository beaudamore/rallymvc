﻿using System;
using eBayData.DAL;
using eBayData.Models;
using RallyMVC.DAL;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Web.Mvc;
using System.Web;

namespace eBayData.Classes
{
    public class ADUserManagement:Controller
    {
        // repos for this controller - make sure to dispose
        private readonly IUserProfileRepository _userProfileRepository; 
        // --

        public ADUserManagement() // initialize the repositories for this controller
        {
            this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// Check on SessionStart if this user has Rally Creds, 
        /// If not, redirect to force creds entry by user
        /// </summary>
        /// <returns></returns>
        public bool IsUserSetup()
        {
            try
            {
                string WithDomain = HttpContext.Current.Request.LogonUserIdentity.Name.ToString();

                // find currently logged in user
                var user = UserPrincipal.Current;
                // check if this user exists in Db with creds
                if (user != null)
                {
                    var u = _userProfileRepository.GetUserProfileByUserSID(user.Sid.ToString());
                    if (u != null
                        && !string.IsNullOrEmpty(u.RallyUsername)
                        && !string.IsNullOrEmpty(u.RallyPassword)
                        && u.IsActive // <-- make sure this person is an active user
                        )
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
                //throw ex;
            }
            return false;
        }

        /// <summary>
        /// Get a list of the AD Users in the specified groups from the web.config keys
        /// that are NOT already in the Db
        /// Used in:
        /// - UserProfile/CreateUserProfile
        /// </summary>
        /// <param name="domainName"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetListOfAdUsersByGroupNotInDb(string domainName, string groupName)
        {
            List<SelectListItem> DDLToReturn = new List<SelectListItem>();
          
            using (var context = new PrincipalContext(ContextType.Domain))
            {
                using (var group = GroupPrincipal.FindByIdentity(context, "Implementation Development"))
                {
                    var users = group.GetMembers(true); // recursively enumerate
                    foreach (var user in users)
                    {
                        if (_userProfileRepository.GetUserProfileByUserSID(user.Sid.ToString()) == null) // only add those who aren't already added
                        {
                            DDLToReturn.Add(new SelectListItem { Value = user.Sid.ToString(), Text = user.DisplayName });
                        }
                    }
                }
            }
            return DDLToReturn;           
        }

        /// <summary>
        /// Get all OUs in respective domain
        /// Used mainly for interrogating AD to figure out what groups/OUs are available.
        /// </summary>
        public static void GetAllOUs()
        {
            // this gets all OU's in a domain
            List<string> orgUnits = new List<string>();
            DirectoryEntry startingPoint = new DirectoryEntry("LDAP://DIRECTNET");
            DirectorySearcher searcher = new DirectorySearcher(startingPoint);
            searcher.Filter = "(objectCategory=organizationalUnit)";
            foreach (SearchResult res in searcher.FindAll())
            {
                orgUnits.Add(res.Path);
            }
        }

        // -- end of class
    }



    // AD MANAGEMENT METHODOLOGIES
    // ---------------------------

    // --one way--
    // set up domain context
    //PrincipalContext ctx = new PrincipalContext(ContextType.Domain);
    // find a user
    //UserPrincipal user = UserPrincipal.FindByIdentity(ctx, "SomeUserName");

    // --another way--
    //DirectoryEntry entry = new DirectoryEntry("LDAP://" + domainName); //+ ",DC=com");
    //DirectorySearcher search = new DirectorySearcher(entry);
    //string query = "(&(objectCategory=person)(objectClass=user)(memberOf=*))";
    //search.Filter = query;
    //search.PropertiesToLoad.Add("memberOf");
    //search.PropertiesToLoad.Add("name");
    //search.PropertiesToLoad.Add("userPrincipalName");

    //System.DirectoryServices.SearchResultCollection mySearchResultColl = search.FindAll();            

    //foreach (SearchResult result in mySearchResultColl)
    //{
    //    //result.Properties.PropertyNames
    //    foreach (string prop in result.Properties["memberOf"])
    //    {
    //        if (prop.Contains(groupName))
    //        {
    //            returnDictionary.Add(result.Properties["name"][0].ToString(), result.Properties["userPrincipalName"][0].ToString());
    //        }
    //    }
    //}

}
