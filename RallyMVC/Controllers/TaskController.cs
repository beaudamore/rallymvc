﻿using eBayData.DAL;
using eBayData.Models;
using RallyMVC.Classes;
using RallyMVC.DAL;
using RallyMVC.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace RallyMVC.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;
        public TaskController()
        {
            this._taskRepository = new TaskRepository(new RallyContext());
        }

        /// <summary>
        /// This method is called via jQuery/AJAX from the Feature/Edit view
        /// and returns a Json object.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTaskToEditForFeatureEdit(int id)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            Task us = _taskRepository.GetTaskById(id);
            TaskCreateEditViewModel model = new TaskCreateEditViewModel
            {
                Description = us.Description,
                Id = us.Id,
                Name = us.Name,
                Notes = us.Notes,
                EstimatedTime = us.EstimatedTime,
                AssignedUserId = us.UserProfileId
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// This method is an 'Upsert'.. insert/update depending on if the item exists.
        /// It is called from Feature/Edit view's "myTaskModal" via jQuery/AJAX.
        /// On error, it returns the error message in the 
        /// Json object for good User Feedback
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpsertTaskToUserStoryForFeatureEdit(TaskCreateEditViewModel model)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            // return GenericJson obj
            GenericJsonViewModel returnModel = new GenericJsonViewModel();
            returnModel.Result = 1; //true/false 1/0
            returnModel.Message = string.Empty;

            // try it
            try
            {
                if (model.Id == null) // its new
                {
                    eBayData.Models.Task newTask = new Task()
                    {
                        UserStoryId = model.UserStoryId,
                        Name = model.Name,
                        Description = model.Description,
                        Notes = model.Notes,
                        EstimatedTime = model.EstimatedTime,
                        UserProfileId = model.AssignedUserId,
                        ScheduleStateTask = 0 // <- this may not be needed; defaults?
                    };
                    _taskRepository.InsertTask(newTask);
                    _taskRepository.Save();
                }
                else // update existing
                {
                    // -- validate ID passed in
                    var taskId = -1;
                    try
                    {
                        taskId = Convert.ToInt32(model.Id);
                    }
                    catch (Exception ex)
                    {
                        using (ErrorLogger errLogger = new ErrorLogger()) // log it
                        {
                            errLogger.LogError(ex, User.Identity.Name);
                        }
                        returnModel.Result = 0;
                        returnModel.Message = ex.Message;
                        // TODO: handle this in the AJAX/jQuery on the page !!
                        //throw;
                    }
                    // -- update existing Task
                    Task t = _taskRepository.GetTaskById(taskId);
                    t.Description = model.Description;
                    t.Name = model.Name;
                    t.Notes = model.Notes;
                    t.EstimatedTime = model.EstimatedTime;
                    t.UserProfileId = model.AssignedUserId;
                    // set returnModel result to true/1
                    returnModel.Result = 1;
                    // repo
                    _taskRepository.UpdateTask(t);
                    _taskRepository.Save();

                }
                return Json(returnModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // log error
                using (ErrorLogger errLogger = new ErrorLogger())
                {
                    errLogger.LogError(ex, User.Identity.Name);
                }
                returnModel.Result = 0;
                returnModel.Message = ex.Message;
                return Json(returnModel, JsonRequestBehavior.AllowGet);
            }
        }
        
        /// <summary>
        /// Deletes the Task from the UserStory
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteTaskFromUserStoryForFeatureEdit(int id)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            // return GenericJson obj
            GenericJsonViewModel returnModel = new GenericJsonViewModel();
            returnModel.Result = 1; //true/false 1/0
            returnModel.Message = string.Empty;

            try
            {
                _taskRepository.DeleteTask(id);
                _taskRepository.Save();
                return Json(returnModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // TODO: do something with ex?...
                returnModel.Result = 0;
                returnModel.Message = ex.Message;
                return Json(returnModel, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _taskRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
