﻿using eBayData.DAL;
using Rally.RestApi;
using Rally.RestApi.Json;
using Rally.RestApi.Response;
using RallyMVC.Classes;
using RallyMVC.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using RallyMVC.ViewModels;
using System.Security.Authentication;
using System.Linq;
using RallyMVC.Filters;

namespace RallyMVC.Controllers
{
    [RallyCredsValidateFilter]
    public class RallyApiController : Controller
    {
        // setup repos
        private readonly IFeatureRepository _featureRepository;
        private readonly IUserStoryRepository _userStoryRepository;
        private readonly IEpicStoryRepository _epicStoryRepository;
        private readonly ITaskRepository _taskRepository;

        public RallyApiController()
        {
            this._featureRepository = new FeatureRepository(new RallyContext());
            this._userStoryRepository = new UserStoryRepository(new RallyContext());
            this._epicStoryRepository = new EpicStoryRepository(new RallyContext());
            this._taskRepository = new TaskRepository(new RallyContext());
        }

        /// <summary>
        /// Main RallyAPI execution page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
      
        /// <summary>
        /// This is the form that creates the Feature in Rally's API
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Create(int id)
        {
            // get feature
            var feature = _featureRepository.GetFeatureById(id);

            // depending on what Feature Type
            // change page to reflect 
            // textbox and 'search' button to confirm existence of US id entered via AJAX
            // which then enables the 'create in Rally API' button
            
            // build model for display
            UserStoryEpicStoryDisplayViewModel storyModel = new UserStoryEpicStoryDisplayViewModel()
            {
                EpicParentStories = feature.EpicStories,
                StandAloneUserStories = _userStoryRepository.GetUserStoriesByFeatureId(id)
                                                            .Where(x => x.EpicStoryId == null)
                                                            .ToList()
            };
            // create view model
            RallyCreateFeatureViewModel model = new RallyCreateFeatureViewModel()
            {
                FeatureId = id,
                FeatureName = feature.Name,
                FeatureType = feature.FeatureType,
                FeatureTypeId = feature.FeatureTypeId,
                StoryModel = storyModel,
                NewFeatureName = string.Empty,
                NewFeatureNamePrefix = string.Empty
            };

            return View(model);
        }
        
        // ASync Processing/progress bar
        delegate string ProcessTask(RallyCreateFeatureViewModel model);

        // create RallyAPIOperations class instance
        RallyApiOperations rallyObj = new RallyApiOperations();

        /// <summary>
        /// This calls the method to create a new Feature in RallyAPI via
        /// RallyAPIOperations.cs class's 'CreateFeatureInRallyFromDb' method.
        /// The user is again validated to have creds before trying to create.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(RallyCreateFeatureViewModel model)
        {
            // verify this user is setup and has permissions
            AdUserManagement ADum = new AdUserManagement();
            if (!ADum.IsUserSetup())
            {
                throw new AuthenticationException();
            }
            // else try to create in Rally
            try
            {
                // ASync Processing
                rallyObj.Add(model.TaskId);
                ProcessTask processTask = new ProcessTask(rallyObj.CreateFeatureInRallyFromDb);
                processTask.BeginInvoke(model, new AsyncCallback(EndLongRunningProcess), processTask);

                // this is now ASYNC--- XTODOX: do something with this returned reference maybe?
                //var newFeatureReference = rallyObj.CreateFeatureInRallyFromDb(model);

                // send feedback
                TempData["Message"] = "Success: Created New Feature '" + model.NewFeatureName + "' in the RallyAPI. ";

                return RedirectToAction("Index", "Feature");
            }
            catch (Exception ex)
            {
                // log error
                using (ErrorLogger errLogger = new ErrorLogger())
                {
                    errLogger.LogError(ex, User.Identity.Name);
                }
                // send feedback
                TempData["Message"] = "Error: "+
                    "There was a problem creating this feature in the RallyAPI. " +
                    "Have you entered your Rally Credentials and are you 'active'? "+
                    "An Error has been logged in the system.";

                return RedirectToAction("MyRallyCreds", "UserProfile");
            }
        }

        //--------------------------------ASYNC-----------------------------------------------------------------
        /// <summary>
        /// Ends the long running process.
        /// </summary>
        /// <param name="result">The result.</param>
        public void EndLongRunningProcess(IAsyncResult result)
        {
            ProcessTask processTask = (ProcessTask)result.AsyncState;
            string id = processTask.EndInvoke(result);
            rallyObj.Remove(id);
        }

        /// <summary>
        /// Gets the current progress.
        /// </summary>
        /// <param name="id">The id.</param>
        public ContentResult GetCurrentProgress(string id)
        {
            this.ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
            var currentProgress = rallyObj.GetStatus(id).ToString();
            return Content(currentProgress);
        }
        //--------------------------------------------------------------------------------------------------------
        
        public SelectList GetListOfWorkSpaces()
        {
            // get list from class
            RallyApiOperations rallyObj = new RallyApiOperations();
            var AvailableWorkspaces = rallyObj.GetListOfWorkspaces();
            // assign list from dictionary
            SelectList returnSelectList = new SelectList(AvailableWorkspaces, "Key", "Value", null);

            return returnSelectList;
        }
     
        /// <summary>
        /// This returns available Projects from the RallyAPI for use in a DDL in Epic/UserStory editing.
        /// </summary>
        /// <returns></returns>
        public SelectList GetListOfProjects()
        {
            // get list from class
            RallyApiOperations rallyObj = new RallyApiOperations();
            var AvailableProjects = rallyObj.GetListOfProjects();
            // assign list from dictionary
            SelectList returnSelectList = new SelectList(AvailableProjects, "Key", "Value", null);

            return returnSelectList;
        }

        public SelectList GetListOfAvailableUsers()
        {
            // get list from class
            RallyApiOperations rallyObj = new RallyApiOperations();
            var AvailableProjects = rallyObj.GetAvailableUsers();
            // assign list from dictionary
            SelectList returnSelectList = new SelectList(AvailableProjects, "Key", "Value", null);

            return returnSelectList;
        }

        public SelectList GetListOfPreliminaryEstimates()
        {
            // get list from class
            RallyApiOperations rallyObj = new RallyApiOperations();
            var AvailableProjects = rallyObj.GetListOfPreliminaryEstimates();
            // assign list from dictionary
            SelectList returnSelectList = new SelectList(AvailableProjects, "Key", "Value", null);

            return returnSelectList;
        }

        /// <summary>
        /// This returns a JsonResultViewModel from the RallyAPIController 
        /// </summary>
        /// <param name="FeatureId"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public ActionResult GetFeatureReferenceById(string FeatureId)
        {
            // get list from class
            RallyApiOperations rallyObj = new RallyApiOperations();
            var featureReference = rallyObj.GetFeatureReferenceAndNameById(FeatureId);

            return Json(featureReference, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _featureRepository.Dispose();
                _epicStoryRepository.Dispose();
                _userStoryRepository.Dispose();
                _taskRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        // ----TEST METHODS ----

        //public ActionResult CreateTestUserStory()
        //{
        //    RallyApiOperations rallyObj = new RallyApiOperations();
        //    rallyObj.CreateTestUserStory();
        //    return View();
        //}
        //public ActionResult CreateTestPortfolioItem()
        //{
        //    RallyApiOperations rallyObj = new RallyApiOperations();
        //    rallyObj.CreateTestPortfolioItem();
        //    return View();
        //}
        //public ActionResult SAMPLE(int id)
        //{
        //    //string username = "bdmaore@ebay.com";
        //    //string password = "2003Mu5t@ng";
        //    string username = ConfigurationManager.AppSettings["RallyAPIUsername"];
        //    string password = ConfigurationManager.AppSettings["RallyAPIPassword"];
        //    string serverUrl = ConfigurationManager.AppSettings["RallyAPIURL"];

        //    // Initialize the REST API. You can specify a web service version if needed in the constructor.
        //    RallyRestApi restApi = new RallyRestApi();
        //    restApi.Authenticate(username, password, serverUrl, proxy: null, allowSSO: false);

        //    //Create an item
        //    DynamicJsonObject toCreate = new DynamicJsonObject();
        //    toCreate["Name"] = "My Defect";
        //    CreateResult createResult = restApi.Create("defect", toCreate);

        //    //Update the item
        //    DynamicJsonObject toUpdate = new DynamicJsonObject();
        //    toUpdate["Description"] = "This is my defect.";
        //    OperationResult updateResult = restApi.Update(createResult.Reference, toUpdate);

        //    //Get the item
        //    DynamicJsonObject item = restApi.GetByReference(createResult.Reference);

        //    //Query for items
        //    Request request = new Request("defect");
        //    request.Fetch = new List<string>() { "Name", "Description", "FormattedID" };
        //    request.Query = new Query("Name", Query.Operator.Equals, "My Defect");
        //    QueryResult queryResult = restApi.Query(request);
        //    foreach (var result in queryResult.Results)
        //    {
        //        //Process item as needed
        //    }

        //    //Delete the item
        //    OperationResult deleteResult = restApi.Delete(createResult.Reference);

        //    return View();

        //}

        // ----END TEST METHODS----

    }
}
