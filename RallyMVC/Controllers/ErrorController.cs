﻿using System.Web.Mvc;
using RallyMVC.ViewModels;

namespace RallyMVC.Controllers
{
    public class ErrorController : Controller
    {
        // GET: ErrorLog
        [HttpGet]
        public ActionResult Index(ErrorViewModel model)
        {
            return View(model);
        }
    }
}
