﻿using System.Web.Mvc;
using System.Web.Routing;
using RallyMVC.Classes;

namespace RallyMVC.Controllers
{
    public class HomeController : Controller
    {
        //private readonly IUserProfileRepository _userProfileRepository;

        public HomeController()
        {
            //this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// DEPRECATED: Verify that the user has creds setup in the local Db. If not, redirect...
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            // DEPRECATED: this now handled in filter RallyCredsValidateFilter
            //var aDum = new AdUserManagement();
            //if (!aDum.IsUserSetup())
            //{
            //    Response.RedirectToRoute(
            //                    new RouteValueDictionary {
            //                        { "Controller", "UserProfile" },
            //                        { "Action", "MyRallyCreds" }});
            //}
            return View();
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}