﻿using eBayData.DAL;
using eBayData.Models;
using RallyMVC.Classes;
using RallyMVC.DAL;
using RallyMVC.Filters;
using RallyMVC.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;

namespace RallyMVC.Controllers
{
    //[AuthorizeAD(Groups = ConfigurationManager.AppSettings["AllowedOUs"])]
    //[AuthorizeAd]
    public class UserProfileController : Controller
    {
        // repos
        private readonly IUserProfileRepository _userProfileRepository;

        public UserProfileController()
        {
            this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// Gets UserProfiles for display
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //ADUserManagement.GetAllOUs();

            List<UserProfileIndexListViewModel> model = _userProfileRepository.GetUserProfiles()
                .Select(x => new UserProfileIndexListViewModel()
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    IsActive = x.IsActive
                })
                .ToList();

            // this is how one combines enum values
            //var test = Enums.UserPermissionFlags.Delete | Enums.UserPermissionFlags.Add | Enums.UserPermissionFlags.Edit;

            return View(model);
            //return View();
        }

        /// <summary>
        /// show a form here to allow user to enter their creds
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult MyRallyCreds()
        {
            // find currently logged in user
            UserPrincipal adUser = null;
            using (HostingEnvironment.Impersonate())
            {
                var userContext = System.Web.HttpContext.Current.User.Identity;
                PrincipalContext ctx = new PrincipalContext(ContextType.Machine, "Dev10", null,
                    ContextOptions.Negotiate);
                adUser = UserPrincipal.FindByIdentity(ctx, userContext.Name);
            }
       
            // pass current user DisplayName to form
            UpsertUserProfileViewModel viewModel = new UpsertUserProfileViewModel()
            {
                Sid = adUser.Sid.Value,
                FullName = adUser.DisplayName,
                IsActive = true
            };
            return View(viewModel);
            //return View();
        }

        /// <summary>
        /// first, check if their creds work in Rally with a simple call,
        /// then, create new record in our Db with Rally Creds,
        /// if successful foward to main landing page.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult MyRallyCreds(UpsertUserProfileViewModel model)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            
            if (UpsertUserProfile(model)) // try upserting
            {
                TempData["Message"] = "Success: Credential Update Succeeded.";
                return RedirectToAction("Index", "Home");
            }
           
            // FAILED: 
            ViewBag.Message = "Updating failed, please contact your Manager.";
            return View(model);
           
        }


        /// <summary>
        /// Another 'upsert' which is an Insert/Update depending if the obj exists
        /// Depending on what vars are passed, it knows if it's the Admin/Mgr 
        /// updating a user or the user themself
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool UpsertUserProfile(UpsertUserProfileViewModel model)
        {
            var returnVal = false;

            // find currently logged in user
            UserPrincipal adUser = null;
            using (HostingEnvironment.Impersonate())
            {
                var userContext = System.Web.HttpContext.Current.User.Identity;
                PrincipalContext ctx = new PrincipalContext(ContextType.Machine, "Dev10", null,
                    ContextOptions.Negotiate);
                adUser = UserPrincipal.FindByIdentity(ctx, userContext.Name);
            }
            UserProfile existingUser = null;
         
            /* 
            UserCredsCheck
            Compare user entered creds against RallyAPI to validate creds
            if bad, return false
            */
            // setup model
            var credsModel = new RallyUserCredsViewModel()
            {
                UserName = model.RallyUsername,
                Password = model.RallyPassword
            };
            // test creds in RallyAPI
            // ----------------------
            RallyApiOperations rallyObj = new RallyApiOperations();
            var rallyUserCredsTestResult = rallyObj.TestUserCreds(credsModel);
            if (rallyUserCredsTestResult == "false") // bad creds
            {
                return false; // exit
            }            
            // ---------------------------
            // --- End UserCreds Check ---


            // --- Upsert User info ---
            // Upon validation, get back the _ref for this user in their system and store in local Db

            if (!string.IsNullOrEmpty(model.UserToCreateId)) // this is an Admin creating a user
            {
                // check if the entry exists, if so, update it.
                existingUser = _userProfileRepository.GetUserProfileByUserSID(model.Sid.ToString());
                if (existingUser != null)
                {
                    // update Existing UserProfile 
                    existingUser.RallyUsername = model.RallyUsername;
                    existingUser.RallyPassword = model.RallyPassword;
                    existingUser.IsActive = model.IsActive;
                    existingUser.RallyReference = rallyUserCredsTestResult;
                    // tell repo
                    _userProfileRepository.UpdateUserProfile(existingUser);
                    _userProfileRepository.Save();
                    returnVal = true;
                }
                else
                {
                    //create new UserProfile in Db
                    var up = new UserProfile()
                    {
                        FullName = model.UserToCreateFullName,
                        RallyUsername = model.RallyUsername,
                        RallyPassword = model.RallyPassword,
                        SID = model.UserToCreateId,
                        IsActive = model.IsActive,
                        RallyReference = rallyUserCredsTestResult
                    };
                    _userProfileRepository.InsertUserProfile(up);
                    _userProfileRepository.Save();
                    returnVal = true;
                }
            }
            else // it's the user themself updating their creds
            {
                if (!string.IsNullOrEmpty(model.Sid)) // SID passed
                { 
                    // check if the entry exists, if so, update it.
                    existingUser = _userProfileRepository.GetUserProfileByUserSID(model.Sid.ToString());
                    if (existingUser != null)
                    {
                        // update Existing UserProfile 
                        existingUser.RallyUsername = model.RallyUsername;
                        existingUser.RallyPassword = model.RallyPassword;
                        existingUser.IsActive = model.IsActive;
                        existingUser.RallyReference = rallyUserCredsTestResult;
                        // tell repo
                        _userProfileRepository.UpdateUserProfile(existingUser);
                        _userProfileRepository.Save();
                        returnVal = true;
                    }
                    else
                    {
                        // create new UserProfile in Db
                        var up = new UserProfile()
                        {
                            FullName = model.FullName,
                            RallyUsername = model.RallyUsername,
                            RallyPassword = model.RallyPassword,
                            SID = model.Sid.ToString(),
                            IsActive = model.IsActive,
                            RallyReference = rallyUserCredsTestResult
                        };
                        _userProfileRepository.InsertUserProfile(up);
                        _userProfileRepository.Save();
                        returnVal = true;
                    }
                }
                else if (adUser != null)
                {
                    // create new UserProfile in Db
                    var up = new UserProfile()
                    {
                        FullName = model.FullName,
                        RallyUsername = model.RallyUsername,
                        RallyPassword = model.RallyPassword,
                        SID = adUser.Sid.ToString(),
                        IsActive = model.IsActive,
                        RallyReference = rallyUserCredsTestResult
                    };
                    _userProfileRepository.InsertUserProfile(up);
                    _userProfileRepository.Save();
                    returnVal = true;
                }
            }

            return returnVal;
        }

        /// <summary>
        /// Create a new user: ADMIN ONLY !!!
        /// shows form and only gets Users form AD that are in the OU 
        /// set in the Web.Config keys as shown below
        /// </summary>
        /// <returns></returns>
        // GET: UserProfile/Create
        //[AuthorizeAD(Groups = "Implementation Development, ")] // --TODO: update to "manager's" group here
        [RallyCredsValidateFilter]
        public ActionResult Create()
        {
            // get list of users in our domain/group and fill the view model           
            AdUserManagement ADUserManagementObj = new AdUserManagement();
            UpsertUserProfileViewModel model = new UpsertUserProfileViewModel
            {
                AdUsers = ADUserManagementObj.GetListOfAdUsersByGroupNotInDb(
                    WebConfigurationManager.AppSettings["AllowedDomain"],
                    WebConfigurationManager.AppSettings["AllowedOUs"])
            };

            return View(model);
        }

        /// <summary>
        /// Posting a new user to create
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        /// TODO: this next line limits who can create a new user
        //[AuthorizeAD(Groups = "Implementation Development, ")] // --TODO: update to "manager's" group here
        [RallyCredsValidateFilter]
        [HttpPost]
        public ActionResult Create(UpsertUserProfileViewModel model)
        {
            //try
            //{
                var isComplete = UpsertUserProfile(model);

                if (isComplete)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    // nothing, just return to the page
                }
            //}
            //catch (Exception ex)
            //{
            //    //return View();
            //}
            return View();
        }

        // GET: UserProfile/Edit/5
        [RallyCredsValidateFilter]
        public ActionResult Edit(int id)
        {
            UpsertUserProfileViewModel model = _userProfileRepository.GetUserProfiles()
                .Where(x => x.Id == id)
                .Select(x => new UpsertUserProfileViewModel()
                {
                    Sid = x.SID.ToString(),
                    FullName = x.FullName,
                    RallyUsername = x.RallyUsername,
                    RallyPassword = x.RallyPassword,
                    IsActive = x.IsActive
                })
                .FirstOrDefault();

            return View(model);
        }

        // POST: UserProfile/Edit/5
        [RallyCredsValidateFilter]
        [HttpPost]
        public ActionResult Edit(UpsertUserProfileViewModel model)
        {
            try
            {
                var isComplete = UpsertUserProfile(model);

                if (isComplete)
                {
                    ViewBag.Message = "Success!";
                    return RedirectToAction("Index", "UserProfile");
                }
                else
                {
                    ViewBag.Message = "The credentials you entered didn't work. Try again?";
                    // TODO: userfeedback, just return to the page
                }
                return View(model);
            }
            catch
            {
                // TODO: userfeedback
                return View();
            }
        }

        // GET: UserProfile/Delete/5
        [RallyCredsValidateFilter]
        public ActionResult Delete(int id)
        {
            var user = _userProfileRepository.GetUserProfileById(id);
            return View(user);
        }

        // POST: UserProfile/Delete/5
        [RallyCredsValidateFilter]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                _userProfileRepository.DeleteUserProfile(id);
                _userProfileRepository.Save();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userProfileRepository.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
