﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using eBayData.DAL;
using eBayData.Models;
using RallyMVC.ViewModels;
using RallyMVC.DAL;
using RallyMVC.Classes;

namespace RallyMVC.Controllers
{
    public class UserStoryController : Controller
    {
        // setup repos
        private readonly IFeatureRepository _featureRepository;
        private readonly IUserStoryRepository _userStoryRepository;
        private readonly IEpicStoryRepository _epicStoryRepository;

        public UserStoryController()
        {
            this._featureRepository = new FeatureRepository(new RallyContext());
            this._userStoryRepository = new UserStoryRepository(new RallyContext());
            this._epicStoryRepository = new EpicStoryRepository(new RallyContext());
            //this._taskRepository = new TaskRepository(new RallyContext());
            //this._errorRepository = new ErrorRepository(new RallyContext());
        }

        /// <summary>
        /// This is hit from FeatureController's Modal
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpsertFromFeatureEdit(StoryCreateEditViewModel model)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
           
            // return GenericJson obj
            GenericJsonViewModel returnModel = new GenericJsonViewModel();
            returnModel.Result = 1; //true/false 1/0

            try
            {
                // are we upserting a UserStory or EpicStory?
                if (model.IsEpicStory)
                {
                    // is it new or updating?
                    EpicStory es = _epicStoryRepository.GetEpicStoryById(model.Id ?? 0); // handle nullable int

                    if (es != null) // it exists, update it
                    {
                        es.Description = model.Description;
                        es.Name = model.Name;
                        es.Notes = model.Notes;
                        es.RallyProjectId = model.RallyProjectId;
                        es.RallyProjectName = model.RallyProjectName;
                        es.EstimatedTime = model.EstimatedTime;
                        es.UserProfileId = model.AssignedUserId;

                        _epicStoryRepository.UpdateEpicStory(es);
                        _epicStoryRepository.Save();
                    }
                    else // its new
                    {
                        // last last SortOrderId for this
                        var lastItem = _epicStoryRepository
                                                .GetEpicStoriesByFeatureId(model.FeatureId)
                                                .OrderByDescending(i => i.SortOrderId)
                                                .FirstOrDefault();
                        // get lastSortOrderId
                        var lastSortOrderId = 0; // if it's the first item
                        if (lastItem != null) // if there's a last item, use it and increment
                        {
                            lastSortOrderId = lastItem.SortOrderId + 1;
                        }
                        // new Epic
                        EpicStory newEs = new EpicStory()
                        {
                            FeatureId = model.FeatureId,
                            Description = model.Description,
                            Name = model.Name,
                            RallyProjectId = model.RallyProjectId,
                            RallyProjectName = model.RallyProjectName,
                            EstimatedTime = model.EstimatedTime,
                            UserProfileId = model.AssignedUserId,
                            SortOrderId = lastSortOrderId
                        };
                        _epicStoryRepository.InsertEpicStory(newEs);
                        _epicStoryRepository.Save();
                    }
                }
                else
                {
                    // check if exists
                    UserStory u = _userStoryRepository.GetUserStoryById(model.Id ?? -1);

                    if (u != null) // UserStory exists so, update it
                    {
                        u.Description = model.Description;
                        u.Name = model.Name;
                        u.Notes = model.Notes;
                        u.RallyProjectId = model.RallyProjectId;
                        u.RallyProjectName = model.RallyProjectName;
                        u.EstimatedTime = model.EstimatedTime;
                        u.UserProfileId = model.AssignedUserId;

                        _userStoryRepository.UpdateUserStory(u);
                        _userStoryRepository.Save();
                    }
                    else // it's new!
                    {
                        UserStory newUserStory = new UserStory()
                        {
                            Name = model.Name,
                            Description = model.Description,
                            Notes = model.Notes,
                            FeatureId = model.FeatureId,
                            RallyProjectId = model.RallyProjectId,
                            RallyProjectName = model.RallyProjectName,
                            EstimatedTime = model.EstimatedTime,
                            UserProfileId = model.AssignedUserId
                        };
                        // this is for StandAlone UserStories that have no Epic affiliation
                        if (model.EpicStoryId != 0)
                        {
                            newUserStory.EpicStoryId = model.EpicStoryId;
                        }
                        else
                        {
                            newUserStory.EpicStoryId = null;
                        }

                        _userStoryRepository.InsertUserStory(newUserStory);
                        _userStoryRepository.Save();
                    }
                }
            }
            catch(Exception ex)
            {
                // log error
                using (ErrorLogger errLogger = new ErrorLogger())
                {
                    errLogger.LogError(ex, User.Identity.Name);
                }
                // TODO: this won't be used as this method is being called from jQuery AJAX
                ViewBag["Message"] = "There was an error processing your request.<br />Error: " + ex.Message;
                returnModel.Result = 0;
                returnModel.Message = ex.Message;
                return Json(returnModel);            
            }
          
            return Json(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetListForFeatureEdit(int id)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);          

            if (ModelState.IsValid)
            {
                // setup model
                UserStoryEpicStoryDisplayViewModel model = new UserStoryEpicStoryDisplayViewModel();
                // get IsEpicStory
                model.EpicParentStories = _epicStoryRepository.GetEpicStoriesByFeatureId(id)
                    .ToList();
                // get Stand-Alone UserStories that have no epic affiliation but are in this Feature
                model.StandAloneUserStories = _userStoryRepository.GetUserStoriesByFeatureId(id)
                    .Where(x => x.EpicStoryId == null)
                    .Include(y => y.UserProfile)
                    .ToList();

                return PartialView("_HTMLListOfUserStoriesView", model);
            }
            return Json(new { result = "Something went wrong." });
            //return Content("0");
        }

        /// <summary>
        /// Used by GetListForFeatureEdit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private List<StoryCreateEditViewModel> GetUserStoriesByFeatureId(int id)
        {
            List<UserStory> lUs = _userStoryRepository.GetUserStoriesByFeatureId(id).ToList();

            List<StoryCreateEditViewModel> uList = new List<StoryCreateEditViewModel>();
            
            foreach (var story in lUs)
            {
                StoryCreateEditViewModel m = new StoryCreateEditViewModel
                {
                    Description = story.Description,
                    FeatureId = story.FeatureId,
                    Id = story.Id,
                    Name = story.Name,
                    Notes = story.Notes,
                    Tasks = story.Tasks
                };
                uList.Add(m);
            }            
            return uList;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StoryToEdit"></param>
        /// ViewModel: 'StoryToEdit.UserOrEpic' is either 'UserStory' or 'EpicStory'
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetStoryToEditForFeatureEdit(StoryEditViewModel StoryToEdit)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            // setup return model
            StoryCreateEditViewModel modelToReturn = new StoryCreateEditViewModel();

            try {
                // depending on what was clicked, bring back either a UserStory or an EpicStory
                switch (StoryToEdit.UserOrEpic)
                {
                    case "UserStory":
                        UserStory us = _userStoryRepository.GetUserStoryById(StoryToEdit.Id);
                        modelToReturn.Description = us.Description;
                        modelToReturn.Id = us.Id;
                        modelToReturn.Name = us.Name;
                        modelToReturn.Notes = us.Notes;
                        modelToReturn.EstimatedTime = us.EstimatedTime;
                        modelToReturn.IsEpicStory = false;
                        modelToReturn.RallyProjectId = us.RallyProjectId;
                        modelToReturn.AssignedUserId = us.UserProfileId;
                        break;

                    case "EpicStory":
                        EpicStory es = _epicStoryRepository.GetEpicStoryById(StoryToEdit.Id);
                        modelToReturn.Description = es.Description;
                        modelToReturn.Id = es.Id;
                        modelToReturn.Name = es.Name;
                        modelToReturn.Notes = es.Notes;
                        modelToReturn.EstimatedTime = es.EstimatedTime;
                        modelToReturn.IsEpicStory = true;
                        modelToReturn.RallyProjectId = es.RallyProjectId;
                        modelToReturn.AssignedUserId = es.UserProfileId;
                        break;
                }
                return Json(modelToReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // log error
                using (ErrorLogger errLogger = new ErrorLogger())
                {
                    errLogger.LogError(ex, User.Identity.Name);
                }                
                return Json(new { result = "Something went wrong. Neither Epic Story nor UserStory was passed." });
            }
        }

        [HttpPost]
        public JsonResult DeleteStoryForFeatureEdit(StoryEditViewModel storyToDelete)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            // return GenericJson obj
            GenericJsonViewModel returnModel = new GenericJsonViewModel();
            returnModel.Result = 1; //true/false 1/0
            returnModel.Message = string.Empty;
            
            // setup return var as false first
            //var myData = new { result = "0" };
            
            // got for it
            try
            {
                switch (storyToDelete.UserOrEpic)
                {
                    case "UserStory":
                        // get UserStory
                        UserStory us = _userStoryRepository.GetUserStoryById(storyToDelete.Id);
                        _userStoryRepository.DeleteUserStory(storyToDelete.Id);
                        _userStoryRepository.Save();
                        break;

                    case "EpicStory":
                        // delete EpicStory
                        _epicStoryRepository.DeleteEpicStory(storyToDelete.Id);
                        _epicStoryRepository.Save();
                        break;
                }
            }
            catch (Exception ex)
            {
                // log error
                using (ErrorLogger errLogger = new ErrorLogger())
                {
                    errLogger.LogError(ex, User.Identity.Name);
                }
                returnModel.Result = 0;
                returnModel.Message = ex.Message;
                //return Json(returnModel, JsonRequestBehavior.AllowGet);
            }
           
            // return for ajax
            return Json(returnModel, JsonRequestBehavior.AllowGet);
        }
              
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _featureRepository.Dispose();
                _userStoryRepository.Dispose();
                _epicStoryRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
