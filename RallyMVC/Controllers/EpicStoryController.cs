﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eBayData.DAL;
using eBayData.Models;
using RallyMVC.DAL;

namespace RallyMVC.Controllers
{
    public class EpicStoryController : Controller
    {
        private readonly IEpicStoryRepository _epicStoryRepository;

        public EpicStoryController(
            //IEpicStoryRepository epicStoryRepository
        )
        {
            this._epicStoryRepository = new EpicStoryRepository(new RallyContext());
        }

        // GET: EpicStory
        public ActionResult Index()
        {
            var something =  _epicStoryRepository.GetEpicStories();
            return View(something);
        }

        // GET: EpicStory/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    int theID = Convert.ToInt32(id);
        //    EpicStory epicStory =  _epicStoryRepository.GetEpicStoryById(theID);
        //    if (epicStory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(epicStory);
        //}

        // GET: EpicStory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EpicStory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Name,Content")] EpicStory epicStory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.EpicStories.Add(epicStory);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(epicStory);
        //}

        //// GET: EpicStory/Edit/5
        //public async Task<ActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    EpicStory epicStory = await db.EpicStories.FindAsync(id);
        //    if (epicStory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(epicStory);
        //}

        //// POST: EpicStory/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Content")] EpicStory epicStory)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(epicStory).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(epicStory);
        //}

        //// GET: EpicStory/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    EpicStory epicStory = await db.EpicStories.FindAsync(id);
        //    if (epicStory == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(epicStory);
        //}

        //// POST: EpicStory/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(int id)
        //{
        //    EpicStory epicStory = await db.EpicStories.FindAsync(id);
        //    db.EpicStories.Remove(epicStory);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _epicStoryRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
