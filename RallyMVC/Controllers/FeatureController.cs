﻿using eBayData.DAL;
using eBayData.Models;
using RallyMVC.Classes;
using RallyMVC.DAL;
using RallyMVC.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RallyMVC.Filters;

namespace RallyMVC.Controllers
{
    //[RallyCredsValidateFilter]
    public class FeatureController : Controller
    {
        // setup repos
        private readonly IFeatureRepository _featureRepository;
        private readonly IFeatureTypeRepository _featureTypeRepository;
        private readonly IEpicStoryRepository _epicStoryRepository;
        private readonly IUserStoryRepository _userStoryRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        // for PreliminaryEstimates (HARD CODED)
        private readonly List<SelectListItem> _preliminaryEstimates = SimpleObjects.PreliminaryEstimateItems();

        public FeatureController()
        {
            // init repos
            this._featureRepository = new FeatureRepository(new RallyContext());
            this._featureTypeRepository = new FeatureTypeRepository(new RallyContext());
            this._epicStoryRepository = new EpicStoryRepository(new RallyContext());
            this._userStoryRepository = new UserStoryRepository(new RallyContext());
            this._taskRepository = new TaskRepository(new RallyContext());
            this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// This gets the list of Features that have at least one Task in any Story below it.
        /// We can change this later as Sue mentioned that Tasks might not be mandatory in some instances
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<FeatureListViewModel> model = _featureRepository.GetFeatures()
                .Select(x => new FeatureListViewModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    FeatureTemplateType = x.FeatureType.Name, // for grouping by feature name
                    CanCreateInRally = x.EpicStories.Any(y => y.UserStories.Any(z => z.Tasks.Count > 0))
                                    || x.UserStories.Any(a => a.Tasks.Count > 0) //<- only true if feature has at least 1 Task in User/Epic story
                })
                .ToList();

            return View(model);
        }

        // GET: Feature/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        /// <summary>
        /// Must get and back the AvailableProjects from the RallyAPI
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            // must plass model for UIHint to work even if blank
            FeatureCreateViewModel model = new FeatureCreateViewModel();

            // get available Projects from RallyAPI
            RallyApiOperations rallyObj = new RallyApiOperations();
            var availableProjects = rallyObj.GetListOfProjects();

            // fill in model
            model.AvailableProjectsInRally = availableProjects;
            model.PreliminaryEstimateChoices = _preliminaryEstimates;

            // dropdown
            model.AvailableUsers = _userProfileRepository.GetUserProfiles()
             .Select(x => new SelectListItem()
             {
                 Text = x.FullName,
                 Value = x.Id.ToString()
             }).ToList();

            // dropdown
            model.AvailableFeatureTemplateTypes = _featureTypeRepository.GetFeatureTypes()
                .Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });

            return View(model);
        }

        /// <summary>
        /// Creates the feature, simple.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [RallyCredsValidateFilter]
        [HttpPost]
        public ActionResult Create(FeatureCreateViewModel model)
        {
            Feature f = new Feature()
            {
                FeatureTypeId = model.AssignedFeatureTemplateTypeId,
                Name = model.Name,
                Description = model.Description,
                RallyProjectId = model.RallyProjectId,
                RallyProjectName = model.RallyProjectName,
                PreliminaryEstimateName = model.PreliminaryEstimateName,
                PreliminaryEstimateId = model.PreliminaryEstimateId,
                UserProfileId = model.AssignedUserId
            };
            _featureRepository.InsertFeature(f);
            _featureRepository.Save();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Gets the feature for editing
        /// </summary>
        /// <param name="id"></param> is the FeatureId
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            //RallyApiOperations rallyObj = new RallyApiOperations();
            //var availableProjects = rallyObj.GetListOfProjects();
            var availableProjects = new List<SelectListItem>
            {
                new SelectListItem()
                {
                    Value = "123", Text = "Project Name"
                }
            };

            var feature = _featureRepository.GetFeatureById(id);

            FeatureEditViewModel model = new FeatureEditViewModel
            {
                AvailableProjectsInRally = availableProjects,               // available projects (Rally-side)
                Description = feature.Description,                          // particulars
                Id = feature.Id,
                Name = feature.Name,
                UserStories = feature.UserStories.ToList(),
                RallyProjectId = feature.RallyProjectId,                    // Project info
                RallyProjectName = feature.RallyProjectName,
                PreliminaryEstimateChoices = _preliminaryEstimates,         // Preliminary Estimate
                PreliminaryEstimateId = feature.PreliminaryEstimateId,
                PreliminaryEstimateName = feature.PreliminaryEstimateName,
                AssignedUserId = feature.UserProfileId ?? 0,                // assigned user
                AvailableUsers = _userProfileRepository.GetUserProfiles()   // available users
                    .Select(x => new SelectListItem()
                    {
                        Text = x.FullName,
                        Value = x.Id.ToString()
                    }).ToList(),
                AssignedFeatureTemplateTypeId = feature.FeatureTypeId,
                AvailableFeatureTemplateTypes = _featureTypeRepository.GetFeatureTypes() // available Feature Types
                    .Select(x => new SelectListItem()
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList()
            };

            return View(model);
        }

        /// <summary>
        /// This method Edits/updates the Feature
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(FeatureEditViewModel model)
        {
            //try
            //{
            // get feature
            Feature f = _featureRepository.GetFeatureById(model.Id);
            // update feature
            f.Name = model.Name;
            f.Description = model.Description;
            f.RallyProjectId = model.RallyProjectId;
            f.RallyProjectName = model.RallyProjectName;
            f.FeatureTypeId = model.AssignedFeatureTemplateTypeId;
            // preliminary estimate
            f.PreliminaryEstimateId = model.PreliminaryEstimateId;
            f.PreliminaryEstimateName = !string.IsNullOrEmpty(model.PreliminaryEstimateId) ? model.PreliminaryEstimateName : null;
            // assigned user
            f.UserProfileId = model.AssignedUserId;
            // tell repo
            _featureRepository.UpdateFeature(f);
            _featureRepository.Save();
            // send feedback
            TempData["Message"] = "Success: Updated this Feature.";

            return RedirectToAction("Edit", new
            {
                ID = model.Id
            });
            //}
            //catch
            //{
            //    // send feedback
            //    TempData["Message"] = "Failed Updating this Feature.";
            //    return RedirectToAction("Edit", new
            //    {
            //        ID = model.Id
            //    });
            //}
        }

        // GET: Feature/Delete/5
        public ActionResult Delete(int id)
        {
            var model = _featureRepository.GetFeatureById(id);
            return View(model);
        }

        // POST: Feature/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            //try
            //{
            _featureRepository.DeleteFeature(id);
            _featureRepository.Save();

            return RedirectToAction("Index");
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.Message = ex.InnerException;
            //    return View();
            //}
        }

        public ActionResult SortItems(SortItemsViewModel model)
        {
            // return model
            GenericJsonViewModel returnModel = new GenericJsonViewModel();
            returnModel.Result = 0;

            // what type of sort?
            switch (model.SortUpdateType)
            {
                case "EpicStory":
                    // get the ALL epic stories for this Epic's FeatureId
                    var EpicStories = _epicStoryRepository.GetEpicStoriesByFeatureId(_epicStoryRepository.GetEpicStoryById(model.SortUpdateId).FeatureId);
                    // decrement everyone above the Old Index (except item in question), removing the gap
                    foreach (var ES in EpicStories.Where(x => x.SortOrderId > model.SortUpdateOldIndexPosition && x.Id != model.SortUpdateId))
                    {
                        ES.SortOrderId--;
                    }
                    // increment everyone at the NEW Index and up (except item in question), creating a gap for sorted item in question
                    foreach (var ES in EpicStories.Where(x=>x.SortOrderId >= model.SortUpdateNewIndexPosition && x.Id != model.SortUpdateId))
                    {
                        ES.SortOrderId++;
                    }
                    // finally, adjust the item in question
                    var EpicStoryTarget = _epicStoryRepository.GetEpicStoryById(model.SortUpdateId);
                    EpicStoryTarget.SortOrderId = model.SortUpdateNewIndexPosition;
                    // save repo
                    _epicStoryRepository.Save();
                    returnModel.Result = 1;
                    break;
                case "UserStory":
                    // get the ALL epic stories for this Epic's FeatureId
                    var UserStories = _userStoryRepository.GetUserStoriesByFeatureId(_userStoryRepository.GetUserStoryById(model.SortUpdateId).FeatureId);
                    // decrement everyone above the Old Index (except item in question), removing the gap
                    foreach (var US in UserStories.Where(x => x.SortOrderId > model.SortUpdateOldIndexPosition && x.Id != model.SortUpdateId))
                    {
                        US.SortOrderId--;
                    }
                    // increment everyone at the NEW Index and up (except item in question), creating a gap for sorted item in question
                    foreach (var US in UserStories.Where(x => x.SortOrderId >= model.SortUpdateNewIndexPosition && x.Id != model.SortUpdateId))
                    {
                        US.SortOrderId++;
                    }
                    // finally, adjust the item in question
                    var UserStoryTarget = _userStoryRepository.GetUserStoryById(model.SortUpdateId);
                    UserStoryTarget.SortOrderId = model.SortUpdateNewIndexPosition;
                    // save repo
                    _userStoryRepository.Save();
                    returnModel.Result = 1;
                    break;
                case "Task":
                    // get the ALL epic stories for this Epic's FeatureId
                    var Tasks = _taskRepository.GetTasksByUserStoryId(_taskRepository.GetTaskById(model.SortUpdateId).UserStoryId);
                    // decrement everyone above the Old Index (except item in question), removing the gap
                    foreach (var Tsk in Tasks.Where(x => x.SortOrderId > model.SortUpdateOldIndexPosition && x.Id != model.SortUpdateId))
                    {
                        Tsk.SortOrderId--;
                    }
                    // increment everyone at the NEW Index and up (except item in question), creating a gap for sorted item in question
                    foreach (var Tsk in Tasks.Where(x => x.SortOrderId >= model.SortUpdateNewIndexPosition && x.Id != model.SortUpdateId))
                    {
                        Tsk.SortOrderId++;
                    }
                    // finally, adjust the item in question
                    var TaskTarget = _taskRepository.GetTaskById(model.SortUpdateId);
                    TaskTarget.SortOrderId = model.SortUpdateNewIndexPosition;
                    // save repo
                    _taskRepository.Save();
                    returnModel.Result = 1;
                    break;
            }
         
            return Json(returnModel, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _featureRepository.Dispose();
                _featureTypeRepository.Dispose();
                _epicStoryRepository.Dispose();
                _userStoryRepository.Dispose();
                _taskRepository.Dispose();
                _userProfileRepository.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
