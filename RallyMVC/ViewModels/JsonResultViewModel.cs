﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class JsonResultViewModel
    {
       
        [DisplayName("Message")]
        public string Message { get; set; }

        public JsonFeatureLookupResult JsonFeatureLookupResult { get; set; }
        
    }
       
}
