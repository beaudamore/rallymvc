﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class ErrorViewModel
    {
       
        [DisplayName("Message")]
        public string Message { get; set; }
        
    }
}
