﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class TaskCreateEditViewModel
    {
        public int? Id { get; set; }

        [DisplayName("UserStoryId")]
        public int UserStoryId { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
     
        [DisplayName("Notes")]
        public string Notes { get; set; }

        [DisplayName("Estimated Time")]
        public decimal EstimatedTime { get; set; }

        // assigned user
        //public string AssignedUserName { get; set; }
        public int? AssignedUserId { get; set; }

        //[DisplayName("Schedule State")]
        //public Enums.ScheduleStateStory ScheduleState { get; set; }

    }
}
