﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class SortItemsViewModel
    {
        
        [DisplayName("Sort Update Type")]
        public string SortUpdateType { get; set; }

        [DisplayName("Sort Update Id")]
        public int SortUpdateId { get; set; }

        [DisplayName("Sort Update Old Index Position")]
        public int SortUpdateOldIndexPosition { get; set; }

        [DisplayName("Sort Update New Index Position")]
        public int SortUpdateNewIndexPosition { get; set; }
    }
}
