﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RallyMVC.ViewModels
{
    public class FeatureCreateViewModel
    {
        public IEnumerable<SelectListItem> AvailableProjectsInRally { get; set; }

        // TemplateType Choices
        [DisplayName("Template Type"), Required]
        public IEnumerable<SelectListItem> AvailableFeatureTemplateTypes { get; set; }
        public int AssignedFeatureTemplateTypeId { get; set; }

        public string RallyProjectId { get; set; }
        public string RallyProjectName { get; set; }

        [DisplayName("Name"), Required]
        public string Name { get; set; }

        [DisplayName("Description"), Required]
        public string Description { get; set; }

        // Preliminary Estimate
        [DisplayName("Preliminary Estimate")]
        public IEnumerable<SelectListItem> PreliminaryEstimateChoices { get; set; }

        [DisplayName("Preliminary Estimate Name")]
        public string PreliminaryEstimateName { get; set; }
        public string PreliminaryEstimateId { get; set; }

        [DisplayName("Assignable Users")]
        public IEnumerable<SelectListItem> AvailableUsers { get; set; }

        public string AssignedUserName { get; set; }
        public int? AssignedUserId { get; set; }



        //public Enums.ScheduleStateFeature ScheduleStateFeature { get; set; }

        //[UIHint("YesNo")]
        //public bool IsExpedited { get; set; }

    }
}
