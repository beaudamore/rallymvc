﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class JsonFeatureLookupResult
    {
        public string _ref { get; set; }
        public string Name { get; set; }
    }
}
