﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class RallyCreateViewModel
    {
        public int Id { get; set; }

        [DisplayName("New Feature Name")]
        public string NewFeatureName { get; set; }

        [DisplayName("Feature Id")]
        public int FeatureId { get; set; }

    }
}
