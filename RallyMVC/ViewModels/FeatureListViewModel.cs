﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class FeatureListViewModel
    {
        public int Id { get; set; }

        [DisplayName("Template Type")]
        public string FeatureTemplateType { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("CanCreateInRally")]
        public bool CanCreateInRally { get; set; }
        
    }
}
