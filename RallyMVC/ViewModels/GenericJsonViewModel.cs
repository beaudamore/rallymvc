﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class GenericJsonViewModel
    {
        // this is either 1 or 0 for true or not
        [DisplayName("result")]
        public int Result { get; set; }

        [DisplayName("Error")]
        public string Message { get; set; }
        
    }
}
