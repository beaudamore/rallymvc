﻿using eBayData.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RallyMVC.ViewModels
{
    public class RallyCreateFeatureViewModel
    {
        // this is for submitting the Id back to the controller
        public int FeatureId { get; set; }

        public string FeatureName { get; set; }

        public int FeatureTypeId { get; set; }
        public FeatureType FeatureType { get; set; }

        public string RallyFeatureReference { get; set; }

        public UserStoryEpicStoryDisplayViewModel StoryModel { get; set; }

        [DisplayName("Prefix")]
        public string NewFeatureNamePrefix { get; set; }
        
        [DisplayName("Name"), Required(ErrorMessage = "Please fill out the Name.")]
        public string NewFeatureName { get; set; }

        public string TaskId { get; set; }

    }
}
