﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RallyMVC.ViewModels
{
    public class UserProfileIndexListViewModel
    {
        public int Id { get; set; }

        [DisplayName("Full Name")]
        public string FullName { get; set; }

        [UIHint("YesNo"), DisplayName("Active")]
        public bool IsActive { get; set; }

        //[DisplayName("Permission")]
        //public Enums.UserPermissionFlags UserPermissionFlags { get; set; }

    }
}
