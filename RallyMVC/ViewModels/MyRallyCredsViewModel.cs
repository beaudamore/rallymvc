﻿using eBayData.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RallyMVC.ViewModels
{
    public class MyRallyCredsViewModel
    {
        [DisplayName("Full Name"), Required]
        public string FullName { get; set; }

        [DisplayName("Rally Username"), Required, EmailAddress]
        public string RallyUsername { get; set; }

        [DisplayName("Rally Password"), Required]
        public string RallyPassword { get; set; }

        public string SID { get; set; }

        public bool IsActive { get; set;}

    }
}
