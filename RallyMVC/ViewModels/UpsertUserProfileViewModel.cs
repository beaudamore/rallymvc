﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RallyMVC.ViewModels
{
    public class UpsertUserProfileViewModel
    {
        public IEnumerable<SelectListItem> AdUsers { get; set; }

        [DisplayName("User To Create"), Required(ErrorMessage = "Please select a User.")]
        public string UserToCreateId { get; set; }

        public string UserToCreateFullName { get; set; }

        [DisplayName("Full Name")]
        public string FullName { get; set; }

        public string Sid { get; set; }

        [DisplayName("Rally Username"), Required, EmailAddress]
        public string RallyUsername { get; set; }

        [DisplayName("Rally Password"), Required]
        public string RallyPassword { get; set; }

        [DisplayName("Active")]
        [UIHint("YesNo")]
        public bool IsActive { get; set; }

    }
}
