﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace RallyMVC.ViewModels
{
    public class UserProfileCreateViewModel
    {
        public Dictionary<string, string> AdUsers { get; set; }

        [DisplayName("Full Name")]
        public string AdUserPrincipalName { get; set; }

        [DisplayName("Rally Username")]
        public string RallyUsername { get; set; }

        [DisplayName("Rally Password")]
        public string RallyPassword { get; set; }

        //public Enums.UserPermissionFlags UserPermissionFlags { get; set; }

        [UIHint("YesNo")]
        [DisplayName("Active")]
        public bool IsActive { get; set; }

    }
}
