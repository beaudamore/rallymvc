﻿using System.Collections.Generic;
using eBayData.Models;

namespace RallyMVC.ViewModels
{
    public class UserStoryEpicStoryDisplayViewModel
    {

        public ICollection<EpicStory> EpicParentStories { get; set; }

        public ICollection<UserStory> StandAloneUserStories { get; set; }       


    }
}
