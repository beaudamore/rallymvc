﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class StoryCreateEditViewModel
    {
        public int? Id { get; set; }

        [DisplayName("FeatureId")]
        public int FeatureId { get; set; }

        [DisplayName("EpicStoryId")]
        public int EpicStoryId { get; set; }

        [DisplayName("Sort Order")]
        public int SortOrderId { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
      
        [DisplayName("Notes")]
        public string Notes { get; set; }

        [DisplayName("Estimated Time")]
        public decimal EstimatedTime { get; set; }

        //[Required]
        public bool IsEpicStory { get; set; }

        public ICollection<eBayData.Models.Task> Tasks { get; set; }

        public string RallyProjectId { get; set; }
        public string RallyProjectName { get; set; }

        public int? AssignedUserId { get; set; }


        //[DisplayName("Schedule State")]
        //public Enums.ScheduleStateStory ScheduleState { get; set; }

        //[DisplayName("Plan Estimate")]
        //public decimal PlanEstimate { get; set; }

        //[DisplayName("Task Estimate")]
        //public decimal TaskEstimate { get; set; }




    }
}
