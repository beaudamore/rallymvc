﻿using System.ComponentModel;

namespace RallyMVC.ViewModels
{
    public class RallyUserCredsViewModel
    {
        [DisplayName("Username/Email")]
        public string UserName { get; set; }

        [DisplayName("Password")]
        public string Password { get; set; }

        public string Sid { get; set; }

    }
}
