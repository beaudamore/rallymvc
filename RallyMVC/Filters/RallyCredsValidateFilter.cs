﻿using RallyMVC.Classes;
using System.Web.Mvc;
using System.Web.Routing;

namespace RallyMVC.Filters
{
    internal class RallyCredsValidateFilter : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var aDum = new AdUserManagement();
            if (!aDum.IsUserSetup())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                                        { "Controller", "UserProfile" },
                                        { "Action", "MyRallyCreds" }
                });

            }
        }
    }
}
