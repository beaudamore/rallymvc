﻿using eBayData.DAL;
using RallyMVC.DAL;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace RallyMVC.Filters
{
    public class AuthorizeAdAttribute : AuthorizeAttribute
    {
        private readonly IUserProfileRepository _userProfileRepository;
       
        // hook up to the repositories
        public AuthorizeAdAttribute()
        {
            this._userProfileRepository = new UserProfileRepository(new RallyContext());
        }

        /// <summary>
        /// this comes from the web.config
        /// </summary>
        //public string Groups { get; set; }

        /// <summary>
        /// Override the authorization routine to check if this user is part of 'AllowedOUs' (web.config key; comma delimited)
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext)) return false;

            /* Return true immediately if the authorization is not 
                locked down to any particular AD group */
            //if (string.IsNullOrEmpty(Groups))
            //    return true;

            // Get the AD groups
            //var groups = Groups.Split(',').ToList();

            // get AD groups from web.config
            var groups = WebConfigurationManager.AppSettings["AllowedOUs"].Split(',').ToList();

            // Verify that the user is in the given AD group (if any)
            var context = new PrincipalContext(
                ContextType.Domain,
                WebConfigurationManager.AppSettings["AllowedDomain"]);

            var userPrincipal = UserPrincipal.FindByIdentity(
                context,
                IdentityType.SamAccountName,
                httpContext.User.Identity.Name);
            try
            {
                foreach (var group in groups)
                {
                    if (userPrincipal.IsMemberOf(context,
                        IdentityType.Name,
                        @group))
                    { return true; }
                }                   
            }
            catch //(Exception ex)
            {
                // not in group
                // will fail through to non-authorized page
                // can log 'hack' attempts if necessary
            }
            return false;
        }

        /// <summary>
        /// redirect on failure
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(
        AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var result = new ViewResult
                {
                    ViewName = "_NotAuthorized",
                    MasterName = "_Layout"
                };
                filterContext.Result = result;
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
