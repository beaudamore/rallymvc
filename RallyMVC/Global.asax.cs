﻿using eBayData.Classes;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using eBayData;
using RallyMVC.Classes;
using RallyMVC.Controllers;

namespace RallyMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            var aDum = new AdUserManagement();
            if (!aDum.IsUserSetup())
            {
                Response.RedirectToRoute(
                                new RouteValueDictionary {
                                    { "Controller", "UserProfile" },
                                    { "Action", "MyRallyCreds" }});
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            var myException = Server.GetLastError();

            // exit if null
            if (myException == null)
                return;

// ======= Process exception =======

// ===== Send Email =====
            // get error email addresses
            var emailAddresses = ConfigurationManager.AppSettings["ErrorEmailAddresses"].Split('|');
            var mail = new MailMessage { From = new MailAddress(ConfigurationManager.AppSettings["ErrorEmailAddressFrom"]) };
            // add each error email address
            foreach (var emailString in emailAddresses)
            {
                mail.To.Add(new MailAddress(emailString));
            }
            mail.Subject = "RallyMVC Error at " + DateTime.Now;
            // include stack trace in email
            mail.Body = "The RallyMVC app had an error at " + DateTime.Now + ".\n\nError Description: " + myException.Message + "\n\nStack Trace:\n" + myException.StackTrace;
            var server = new SmtpClient { Host = "lvsprdmailgw.prd.gsi.local" };
            server.Send(mail);

// ===== log to Db (optional) =====
            var errLogger = new ErrorLogger();
            errLogger.LogError(myException, "");
            errLogger.Dispose();

// ===== Write to event viewer =====
            // Make sure the Eventlog Exists
            const string eventLog = "Internal Websites";
            var eventSource = myException.Source;
            if (!EventLog.SourceExists(eventSource))
            {
                EventLog.CreateEventSource(eventSource, eventLog);
            }
            // Create an EventLog instance and assign its source.
            var myLog = new EventLog(eventLog) {Source = eventSource};
            // build log entry info
            var logMessage = "An error occurred in the Web application " + eventSource +
                             "\r\n\r\nMessage: " + myException.Message;
            // add InnerException if available
            if (myException.InnerException != null)
            {
                logMessage += "\r\n\r\nInnerException.Message:\r\n" + (!string.IsNullOrEmpty(myException.InnerException.Message) ? myException.InnerException.Message : "");
            }
            // add stack trace
            logMessage += "\r\n\r\nStack Trace:\r\n" + myException.StackTrace;

            // Write the error entry to the event log. 
            myLog.WriteEntry(logMessage, EventLogEntryType.Error);

// ===== cleanup
            // Clear the error
            //Server.ClearError();

            // Possible that a partially rendered page has already been written to response buffer before encountering error, so clear it.
            //Response.Clear();

// ===== 'Redirect' of-sorts to a landing page =====
            //var routeData = new RouteData();
            //routeData.Values.Add("controller", "Error");
            //routeData.Values.Add("action", "Index");
            //routeData.Values.Add("Message", myException.Message);

            //IController errorController = new ErrorController();
            //errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));

        }

    }
}
