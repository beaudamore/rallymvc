﻿using System;
using System.Collections.Generic;
using System.Web.Hosting;
using System.Web.Mvc;

namespace RallyMVC.Classes
{
    public static class SimpleObjects
    {
        public static List<SelectListItem> PreliminaryEstimateItems()
        {
            var _preliminaryEstimates = new List<SelectListItem>();
            _preliminaryEstimates.Add(new SelectListItem() { Text = "XS (< 100 hrs.)", Value = "https://rally1.rallydev.com/slm/webservice/v2.0/preliminaryestimate/12480729353" });
            _preliminaryEstimates.Add(new SelectListItem() { Text = "S (100-500 hrs.)", Value = "https://rally1.rallydev.com/slm/webservice/v2.0/preliminaryestimate/12480729355" });
            _preliminaryEstimates.Add(new SelectListItem() { Text = "M (500-1500 hrs.)", Value = "https://rally1.rallydev.com/slm/webservice/v2.0/preliminaryestimate/12480729357" });
            _preliminaryEstimates.Add(new SelectListItem() { Text = "L (1500-5000 hrs.)", Value = "https://rally1.rallydev.com/slm/webservice/v2.0/preliminaryestimate/12480729359" });
            _preliminaryEstimates.Add(new SelectListItem() { Text = "XL (5000+ hrs.)", Value = "https://rally1.rallydev.com/slm/webservice/v2.0/preliminaryestimate/12480729361" });
            return _preliminaryEstimates;
        }

        public static string GetUserFullName()
        {
            // find currently logged in user
            using (HostingEnvironment.Impersonate())
            {
                using (
                    var context =
                        new System.DirectoryServices.AccountManagement.PrincipalContext(
                            System.DirectoryServices.AccountManagement.ContextType.Machine))
                {
                    var principal = System.DirectoryServices.AccountManagement.UserPrincipal.FindByIdentity(
                        context, System.Web.HttpContext.Current.User.Identity.Name);
                    //string.Concat(principal.GivenName, " ", principal.Surname);
                    return principal.DisplayName;
                }
            }
        }
    }
}
