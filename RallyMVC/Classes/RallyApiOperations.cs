﻿using eBayData.DAL;
using eBayData.Models;
using Rally.RestApi;
using Rally.RestApi.Json;
using Rally.RestApi.Response;
using RallyMVC.DAL;
using RallyMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;

namespace RallyMVC.Classes
{
    // known constants
    /*
    RallyAPI ProjectIDs:
        PD-VendorNet Implementations =      https://rally1.rallydev.com/slm/webservice/v2.0/project/32709392846
        DT-VN Client Development =          https://rally1.rallydev.com/slm/webservice/v2.0/project/19327805425
        DT-VN Implementation Engineers =    https://rally1.rallydev.com/slm/webservice/v2.0/project/32309312067
        DT-VN Implementation Specialists =  https://rally1.rallydev.com/slm/webservice/v2.0/project/32709897767
        DT-VN Client Solutions =            https://rally1.rallydev.com/slm/webservice/v2.0/project/40616087865
        DT-VN Vendor Onboarding =           https://rally1.rallydev.com/slm/webservice/v2.0/project/44287332562
    */

    public class RallyApiOperations
    {
        #region Locals
        // local repos
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IUserStoryRepository _userStoryRepository;
        private readonly IFeatureRepository _featureRepository;

        // user creds to use in the RallyAPI
        private KeyValuePair<string, string> _userCreds;

        // for aSync/progressbar processing
        private static readonly object SyncRoot = new object();
        /// <summary>
        /// Gets or sets the process status.
        /// </summary>
        /// <value>The process status.</value>
        private static IDictionary<string, int> ProcessStatus { get; set; }
        #endregion

        // Initialization
        #region Initialization
        /// <summary>
        /// Constructor fills _userCreds with the logged in user's Rally creds
        /// stored in the Db.
        /// </summary>
        public RallyApiOperations() // Get user credentials in constructor
        {
            // init repos
            this._userProfileRepository = new UserProfileRepository(new RallyContext());
            this._userStoryRepository = new UserStoryRepository(new RallyContext());
            this._featureRepository = new FeatureRepository(new RallyContext());

            // find currently logged in user
            UserPrincipal user = null;
            using (HostingEnvironment.Impersonate())
            {
                var userContext = System.Web.HttpContext.Current.User.Identity;
                var ctx = new PrincipalContext(ContextType.Machine, "Dev10", null,
                    ContextOptions.Negotiate);
                user = UserPrincipal.FindByIdentity(ctx, userContext.Name);
            }
            // check if this user exists in Db with creds
            UserProfile u = _userProfileRepository.GetUserProfileByUserSID(user.Sid.ToString());
            if (u != null)
            {
                _userCreds = new KeyValuePair<string, string>
                (
                    u.RallyUsername,
                    u.RallyPassword
                );
            }
            else
            {
                // TODO: what to do here when expecting valid user creds?
                //throw new AuthenticationException();
            }

            // for ASync processing
            if (ProcessStatus == null)
            {
                ProcessStatus = new Dictionary<string, int>();
            }
        }

        /// <summary>
        /// This creates/returns a RallyRestAPI object for all API calls
        /// and uses this web.config key: "RallyAPIURL"
        /// </summary>
        /// <returns></returns>
        private RallyRestApi GetRallyRestApi()
        {
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = new RallyRestApi();
            restApi.Authenticate(
                _userCreds.Key,     // username
                _userCreds.Value,   // password
                WebConfigurationManager.AppSettings["RallyAPIURL"], // URL 
                proxy: null,
                allowSSO: false);

            return restApi;
        }

        private RallyRestApi GetRallyRestApi(string userName, string password)
        {
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = new RallyRestApi();
            restApi.Authenticate(
                userName,     // username
                password,   // password
                WebConfigurationManager.AppSettings["RallyAPIURL"], // URL 
                proxy: null,
                allowSSO: false);

            return restApi;
        }
        #endregion

        // === METHODS ===
        #region Methods
        /// <summary>
        /// This gets everything needed to create a new feature from the Db and then posts it to Rally's API
        /// This is hit from RallyAPIController.cs and runs on a background thread. 
        /// So, once started it cannot be stopped by closing the browser.
        /// FYI
        /// </summary>
        /// <param name="model"></param>
        public string CreateFeatureInRallyFromDb(RallyCreateFeatureViewModel model)
        {
            // get Feature 
            var featureToAdd = _featureRepository.GetFeatureById(model.FeatureId);

            #region UNUSED
            // ---| unused - doing sorting in View, WAY easier | ---
            //var unorderedFeature = _featureRepository.GetFeatureById(model.FeatureId);

            //// featureToAdd created from unordereffeature
            //Feature featureToAdd = new Feature()
            //{
            //    Description = unorderedFeature.Description,
            //    EpicStories = unorderedFeature.EpicStories.OrderBy(m => m.Id).ToList(), // order Epic stories
            //    FeatureType = unorderedFeature.FeatureType,
            //    FeatureTypeId = unorderedFeature.FeatureTypeId,
            //    PreliminaryEstimateId = unorderedFeature.PreliminaryEstimateId,
            //    PreliminaryEstimateName = unorderedFeature.PreliminaryEstimateName,
            //    RallyProjectId = unorderedFeature.RallyProjectId,
            //    UserProfileId = unorderedFeature.UserProfileId,
            //    UserStories = unorderedFeature.UserStories.OrderBy(n => n.Id).ToList()  // order User Stories
            //};

            //// -- SORTING --
            //// =============

            //// order all epic story's User Stories
            //foreach (var epicStory in featureToAdd.EpicStories)
            //{
            //    epicStory.UserStories = epicStory.UserStories.OrderBy(x => x.Id).ToList();
            //    // order tasks
            //    foreach (var userStory in epicStory.UserStories)
            //    {
            //        userStory.Tasks = userStory.Tasks.OrderBy(x => x.Id).ToList();
            //    }
            //}

            //// order tasks in Stand-Alone user Stories
            //foreach (var userStory in featureToAdd.UserStories)
            //{
            //    userStory.Tasks = userStory.Tasks.OrderBy(x => x.Id).ToList();
            //}
            #endregion

            // get Stand-Alone UserStories for this Feature based on Id
            var standAloneUserStoriesToAdd = featureToAdd.UserStories
                                                         .Where(x => x.EpicStoryId == null)
                                                         .OrderBy(y => y.SortOrderId);

            // METRICS / Progress Bar --
            // for progress bar feedback, get a count of all operations and increment counter and get % complete
            var epicStoriesCount = featureToAdd.EpicStories.Count();
            var epicStoryTaskCount = featureToAdd.EpicStories.Select(x => x.UserStories).Count();
            var epicUserStoriesCount = featureToAdd.EpicStories.SelectMany(x => x.UserStories).Count();
            var epicUserStoriesTaskCount = featureToAdd.EpicStories.SelectMany(x => x.UserStories).SelectMany(y => y.Tasks).Count();
            var standAloneUserStoriesCount = standAloneUserStoriesToAdd.Count();
            var standAloneUserStoriesTaskCount = standAloneUserStoriesToAdd.Select(x => x.Tasks).Count();

            // sum them up
            decimal totalOperationCount = epicStoriesCount + epicStoryTaskCount + epicUserStoriesCount + epicUserStoriesTaskCount + standAloneUserStoriesCount + standAloneUserStoriesTaskCount;
            // End Metrics --

            // counter
            decimal completedOperationCount = 0;

            // RALLY API -- Initialize the REST API
            RallyRestApi restApi = GetRallyRestApi();

            // Create their Json object for this Feature
            DynamicJsonObject toCreate = new DynamicJsonObject();

            // Use this for the Feature Ref in Rally from either creating or just getting ref for updating
            var RallyFeatureObjectReference = string.Empty;

            /*
            Condition on FeatureType
            */
            switch (featureToAdd.FeatureType.Name) // magic numbers suck, I know...
            {
                case "New Feature":                 // Create New Feature      
                    toCreate["Name"] = AddPrefixToNameIfAvailable(model.NewFeatureName, model.NewFeatureNamePrefix);
                    toCreate["Description"] = featureToAdd.Description;
                    toCreate["Owner"] = featureToAdd.UserProfile != null ? featureToAdd.UserProfile.RallyReference : string.Empty;
                    toCreate["Project"] = featureToAdd.RallyProjectId;
                    toCreate["PreliminaryEstimate"] = featureToAdd.PreliminaryEstimateId;// <- this matches the website's enumeration of 'PreliminaryEstimates'
                    // create feature - (feature is under PortfolioItem) - and set reference var to ref of new feature
                    RallyFeatureObjectReference = restApi.Create("PortfolioItem/Feature", toCreate).Reference;
                    break;

                case "Update Existing Feature":     // Update Existing Feature
                    // set reference var to model field; we already know from verification
                    RallyFeatureObjectReference = model.RallyFeatureReference;
                    break;
            }

            // progressbar
            completedOperationCount++; // increment for Progressbar
            var Percentage = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
            ProcessStatus[model.TaskId.ToString()] = Percentage;   // progressbar
            //Thread.Sleep(350); // TODO: remove these delays?

            // Get the feature just created (not needed here but good to know)
            //DynamicJsonObject item = restApi.GetByReference(RallyFeatureObject.Reference);

            // ------------------------------
            // -- add Stand-Alone UserStories
            // ------------------------------
            foreach (UserStory u in standAloneUserStoriesToAdd.OrderByDescending(x => x.SortOrderId))
            {
                toCreate = new DynamicJsonObject();
                /* 
                put together the name
                from appending a prefix if exists
                */
                var UserStoryName = AddPrefixToNameIfAvailable(u.Name, model.NewFeatureNamePrefix);
                toCreate["Name"] = UserStoryName;
                toCreate["Description"] = u.Description;
                toCreate["Owner"] = u.UserProfile != null ? u.UserProfile.RallyReference : string.Empty ;
                toCreate["PortfolioItem"] = Ref.GetRelativeRef(RallyFeatureObjectReference);
                toCreate["Project"] = u.RallyProjectId;
                toCreate["Estimate"] = u.EstimatedTime;
                //toCreate["DragAndDropRank"] = counterStandAlone;
                CreateResult createStandAloneUserStoryResult = restApi.Create("hierarchicalrequirement", toCreate);

                // progressbar
                completedOperationCount++; // increment for Progressbar
                ProcessStatus[model.TaskId.ToString()] = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
                //Thread.Sleep(350);

                foreach (eBayData.Models.Task t in u.Tasks.OrderBy(x => x.SortOrderId))
                {
                    toCreate = new DynamicJsonObject();
                    /* 
                    put together the name
                    from appending a prefix if exists
                    */
                    var TaskName = AddPrefixToNameIfAvailable(t.Name, model.NewFeatureNamePrefix);
                    toCreate["Name"] = TaskName;
                    toCreate["Description"] = t.Description;
                    toCreate["Owner"] = t.UserProfile != null ? t.UserProfile.RallyReference : string.Empty;
                    toCreate["WorkProduct"] = Ref.GetRelativeRef(createStandAloneUserStoryResult.Reference);
                    toCreate["Estimate"] = t.EstimatedTime;
                    //toCreate["DragAndDropRank"] = counterTask;
                    CreateResult createStandAloneUserStoryTaskResult = restApi.Create("task", toCreate);

                    // progressbar
                    completedOperationCount++; // increment for Progressbar
                    ProcessStatus[model.TaskId.ToString()] = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
                    //Thread.Sleep(350);
                }
            }

            // -------------------------------------------------------------------
            // add Epic Stories (in RallyAPI: hierarchicalrequirement = UserStory)
            // -------------------------------------------------------------------

            // create each Epic
            foreach (EpicStory e in featureToAdd.EpicStories.OrderByDescending(x => x.SortOrderId))
            {
                toCreate = new DynamicJsonObject();
                /* 
                 put together the name
                 from appending a prefix if exists
                */
                var EpicStoryName = AddPrefixToNameIfAvailable(e.Name, model.NewFeatureNamePrefix);
                toCreate["Name"] = EpicStoryName;
                toCreate["Description"] = e.Description;
                toCreate["Owner"] = e.UserProfile != null ? e.UserProfile.RallyReference : string.Empty;
                toCreate["PortfolioItem"] = Ref.GetRelativeRef(RallyFeatureObjectReference);
                toCreate["Project"] = e.RallyProjectId;
                toCreate["Estimate"] = e.EstimatedTime;
                //toCreate["DragAndDropRank"] = counterEpic;
                CreateResult createEpicStoryResult = restApi.Create("hierarchicalrequirement", toCreate);
            
                // progressbar
                completedOperationCount++; // increment for Progressbar
                ProcessStatus[model.TaskId.ToString()] = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
                //Thread.Sleep(350);

                // foreach userstory create the UserStory and each subsequent Task
                foreach (UserStory u in e.UserStories.OrderByDescending(x => x.SortOrderId))
                {
                    toCreate = new DynamicJsonObject();
                    /* 
                     put together the name
                     from appending a prefix if exists
                    */
                    var UserStoryName = AddPrefixToNameIfAvailable(u.Name, model.NewFeatureNamePrefix);
                    toCreate["Name"] = UserStoryName;
                    toCreate["Description"] = u.Description;
                    toCreate["Owner"] = u.UserProfile != null ? u.UserProfile.RallyReference : string.Empty;
                    toCreate["Parent"] = Ref.GetRelativeRef(createEpicStoryResult.Reference);
                    toCreate["Project"] = u.RallyProjectId;
                    toCreate["Estimate"] = u.EstimatedTime;
                    //toCreate["DragAndDropRank"] = counterUserStory;
                    CreateResult createUserStoryResult = restApi.Create("hierarchicalrequirement", toCreate);

                    // progressbar
                    completedOperationCount++; // increment for Progressbar
                    ProcessStatus[model.TaskId.ToString()] = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
                    //Thread.Sleep(350);

                    // -- add tasks for each UserStory

                    // create each task
                    foreach (eBayData.Models.Task t in u.Tasks.OrderBy(x => x.SortOrderId))
                    {
                        toCreate = new DynamicJsonObject();
                        /* 
                         put together the name
                         from appending a prefix if exists
                        */
                        var TaskName = AddPrefixToNameIfAvailable(t.Name, model.NewFeatureNamePrefix);
                        toCreate["Name"] = TaskName;
                        toCreate["Description"] = t.Description;
                        toCreate["Owner"] = t.UserProfile != null ? t.UserProfile.RallyReference : string.Empty;
                        toCreate["WorkProduct"] = Ref.GetRelativeRef(createUserStoryResult.Reference);
                        toCreate["Estimate"] = t.EstimatedTime;
                        //toCreate["DragAndDropRank"] = counterTask;
                        CreateResult createUserStoryTaskResult = restApi.Create("task", toCreate);

                        // progressbar
                        completedOperationCount++; // increment for Progressbar
                        ProcessStatus[model.TaskId.ToString()] = Convert.ToInt32(Math.Round((completedOperationCount / totalOperationCount), 2) * 100);
                        //Thread.Sleep(350);
                    }
                }
            }

            //send back the reference for the new feature
            return RallyFeatureObjectReference;
        }

        /// <summary>
        /// Gets a list of projects for this subscription
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetListOfProjects()
        {
            //var RallyFeatureRef = string.Empty;
            //var RallyFeatureRefObjectUUID = string.Empty;
            //var RallyFeatureFormattedId = string.Empty;

            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            Rally.RestApi.Request request = new Request("project");
            request.Workspace = WebConfigurationManager.AppSettings["RallyAPIWorkspaceID"]; // likely this is not needed since we have only one 'workspace'
            //request.Workspace = "/workspace/12480728748";
            Rally.RestApi.Response.QueryResult queryResult = restApi.Query(request);

            List<SelectListItem> availableProjects = new List<SelectListItem>();

            foreach (var result in queryResult.Results)
            {
                var selectListItem = new SelectListItem
                {
                    Text = result["Name"], // Project Name
                    Value = result["_ref"] // Reference
                };
                availableProjects.Add(selectListItem);
            }

            return availableProjects;
        }

        /// <summary>
        /// Test user entered creds against Rally's API
        /// Returns 'false' or the user's "_ref" in RallyAPI
        /// </summary>
        /// <returns></returns>
        public string TestUserCreds(RallyUserCredsViewModel model)
        {
            // return var
            string returnString = "false";
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = new RallyRestApi();
            restApi.Authenticate(
                model.UserName,
                model.Password,
                WebConfigurationManager.AppSettings["RallyAPIURL"], // URL 
                proxy: null,
                allowSSO: false);
            // if the creds worked, get this user's rally id
            if (restApi.AuthenticationState == RallyRestApi.AuthenticationResult.Authenticated)
            {
                // update returnString with User's _ref
                returnString = GetRallyUserReferenceByModel(restApi, model);
            }

            return returnString;
        }

        /// <summary>
        /// Gets User form RallyAPI and returns _ref as string
        /// </summary>
        /// <returns></returns>
        private static string GetRallyUserReferenceByModel(RallyRestApi restApi, RallyUserCredsViewModel model)
        {
            var returnVar = string.Empty;

            Rally.RestApi.Request userRequest = new Request("user")
            {
                Fetch = new List<string>()
                {
                    "_ref"
                },
                Query = new Query("UserName", Query.Operator.Equals, model.UserName)
                    .And(new Query("Disabled", Query.Operator.Equals, "false")) // Not 'Disabled'
            };

            // query the RallyAPI and just return empty string if not successful
            QueryResult queryUserResults = restApi.Query(userRequest);
            if (!queryUserResults.Success) return returnVar;

            // return result
            var result = queryUserResults.Results.FirstOrDefault();
            returnVar = result["_ref"]; // let it bubble up if error/null/blank
            return returnVar;
        }

        /// <summary>
        /// Gets all available users in the Rally API
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetAvailableUsers()
        {
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            Rally.RestApi.Request userRequest = new Request("user");

            userRequest.Fetch = new List<string>()
                {
                   //"Role",
                   //"CostCenter",
                   //"LastLoginDate",
                   //"OfficeLocation",
                   //"CreationDate"
                   "UserName",
                   "DisplayName",
                   "Disabled",
                   //"UserPermissions"
                };

            // === FILTERS ===
            /* 
            TODO: do we want everyone from all available 'projects'? IF not, uncomment this and pick the Project
            FYI: this still returns the same amount of users with or without this next line commented
            FYI: See top of this class for Project enumeration
            */
            //userRequest.Project = "https://rally1.rallydev.com/slm/webservice/v2.0/project/32309312067"; // (hard coded: DT-VN Implementation Engineers)

            /*
            ROLES in RALLYAPI: (according to their docs @ https://rally1.rallydev.com/slm/doc/webservice/)
            "Product Manager", "Product Owner", "Scrum Master", "Architect", "Developer", "Test Developer", "Technical Writer", "User Experience"
            */

            // get by Role(s) && not Disabled
            //userRequest.Query = new Query("Project", Query.Operator.Equals, "https://rally1.rallydev.com/slm/webservice/v2.0/project/32309312067");       // Role is 'Developer'

            userRequest.Query = new Query("Role", Query.Operator.Equals, "Developer")       // Role is 'Developer'
                           .Or(new Query("Role", Query.Operator.Equals, "Test Developer"))  // Role is 'Test Developer'
                           .And(new Query("Disabled", Query.Operator.Equals, "false"));     // Not 'Disabled'

            // === end FILTERS ===

            // query the RallyAPI
            QueryResult queryUserResults = restApi.Query(userRequest);

            // return var
            List<SelectListItem> AvailableUsers = new List<SelectListItem>();

            // fill return var from API results
            foreach (var result in queryUserResults.Results)
            {
                SelectListItem selectListItem = new SelectListItem();

                if (!string.IsNullOrEmpty(result["DisplayName"])) //If DisplayName is blank, use UserName
                    selectListItem.Text = result["DisplayName"];
                else
                    selectListItem.Text = result["UserName"];

                selectListItem.Value = result["_ref"]; // ref in RallyAPI
                AvailableUsers.Add(selectListItem);
            }

            return AvailableUsers;
        }

        public JsonResultViewModel GetFeatureReferenceAndNameById(string FeatureIdToSearchFor)
        {
            // init API
            RallyRestApi restApi = GetRallyRestApi();

            // return obj
            JsonResultViewModel returnModel = new JsonResultViewModel();

            Rally.RestApi.Request featureRequest = new Request("PortfolioItem/Feature")// <<--------- TODO: figure out what this is in RALLYAPI
            {
                Fetch = new List<string>()
                {
                    "_ref",
                    "Name"
                },
                Query = new Query("FormattedID", Query.Operator.Equals, FeatureIdToSearchFor) // this might not be the right key   
            };

            // query the RallyAPI and just return empty string if not successful
            var queryFeatureResults = restApi.Query(featureRequest);

            if (!queryFeatureResults.Success)
            {
                returnModel.Message = queryFeatureResults.Errors.ToString();
            }
            else
            {
                if (queryFeatureResults.Results.Count() > 0)
                {
                    var result = queryFeatureResults.Results.FirstOrDefault();
                    var JsonFeatureLookupResult = new JsonFeatureLookupResult()
                    {
                        _ref = result["_ref"],
                        Name = result["Name"]
                    };
                    returnModel.JsonFeatureLookupResult = JsonFeatureLookupResult;
                }
                else
                {
                    returnModel.Message = "Feature Not Found";
                }

            }
            // return result
            return returnModel;
        }

        /// <summary>
        /// Gets a list of Workspaces, of which we are only using one anyway
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetListOfWorkspaces()
        {
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            Rally.RestApi.Request request = new Request("Workspaces");
            Rally.RestApi.Response.QueryResult queryResult = restApi.Query(request);

            List<SelectListItem> AvailableWorkspaces = new List<SelectListItem>();

            foreach (var result in queryResult.Results)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = result["Name"];  // workspaceName
                selectListItem.Value = result["_ref"]; // workspaceRef
                AvailableWorkspaces.Add(selectListItem);
            }

            return AvailableWorkspaces;
        }


        /// <summary>
        /// Gets the hardcoded PReliminary Estimates in Rally.
        /// I am using these as hardcoded in the initialization of this class.
        /// ...Really a one-time use method/
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetListOfPreliminaryEstimates()
        {
            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            Rally.RestApi.Request request = new Request("PreliminaryEstimate");
            Rally.RestApi.Response.QueryResult queryResult = restApi.Query(request);

            List<SelectListItem> availablePreliminaryEstimates = new List<SelectListItem>();

            foreach (var result in queryResult.Results)
            {
                SelectListItem selectListItem = new SelectListItem
                {
                    Text = result["Name"], // workspaceName
                    Value = result["_ref"] // workspaceRef
                };
                availablePreliminaryEstimates.Add(selectListItem);
            }

            return availablePreliminaryEstimates;
        }

        #endregion

        // === ASync Processing ===
        #region ASync Processing
        /// <summary>
        /// Adds the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        public void Add(string id)
        {
            lock (SyncRoot)
            {
                ProcessStatus.Add(id, 0);
            }
        }

        /// <summary>
        /// Removes the specified id.
        /// </summary>
        /// <param name="id">The id.</param>
        public void Remove(string id)
        {
            lock (SyncRoot)
            {
                ProcessStatus.Remove(id);
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <param name="id">The id.</param>
        public int GetStatus(string id)
        {
            lock (SyncRoot)
            {
                if (ProcessStatus.Keys.Count(x => x == id) == 1)
                {
                    return ProcessStatus[id];
                }
                else
                {
                    return 100;
                }
            }
        }
        #endregion

        // == Helper Methods ==
        #region Helper Methods
        /// <summary>
        /// Method to append prefix if is exists (not null or empty string)
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Prefix"></param>
        /// <returns>string Prefixed Name</returns>
        private string AddPrefixToNameIfAvailable(string Name, string Prefix = null)
        {
            var nameString = string.Empty;
            if (String.IsNullOrEmpty(Prefix)) // if prefix empty, just use name
            {
                nameString = Name;
            }
            else // otherwise use prefix + '-' + name
            {
                nameString = Prefix + "-" + Name;
            }
            return nameString;
        }
        #endregion

        // === TEST METHODS ===
        #region Test Methods
        /// <summary>
        /// just for testing
        /// </summary>
        /// <returns></returns>
        public int CreateTestPortfolioItem()
        {
            var RallyFeatureRef = string.Empty;
            var RallyFeatureRefObjectUUID = string.Empty;
            var RallyFeatureFormattedId = string.Empty;

            var FeatureToAdd = _featureRepository.GetFeatures().FirstOrDefault(x => x.Id == 1);    // <- hard coded TODO: correct this in production      

            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            //Create an item
            DynamicJsonObject toCreate = new DynamicJsonObject();
            toCreate["Name"] = FeatureToAdd.Name;
            toCreate["Description"] = FeatureToAdd.Description;

            // important to which this belongs
            //toCreate["Workspace"] = "/workspace/" + WebConfigurationManager.AppSettings["RallyAPIWorkspaceID"];
            //toCreate["Project"] = "/project/32709392846d";
            //toCreate["Iteration"] = "/iteration/4459106059";

            // create feature - feature is under PortfolioItem
            CreateResult createFeatureResult = restApi.Create("PortfolioItem/Feature", toCreate);

            // Get the feature just created
            //DynamicJsonObject item = restApi.GetByReference(createFeatureResult.Reference);

            // -- add UserStories
            foreach (UserStory u in FeatureToAdd.UserStories)
            {
                toCreate = new DynamicJsonObject();
                toCreate["Name"] = u.Name;
                toCreate["Description"] = u.Description;
                toCreate["PortfolioItem"] = Ref.GetRelativeRef(createFeatureResult.Reference);
                // hierarchicalrequirement = UserStory
                CreateResult createUserStoryResult = restApi.Create("hierarchicalrequirement", toCreate);

                // -- add tasks for each UserStory
                foreach (eBayData.Models.Task t in u.Tasks)
                {
                    toCreate = new DynamicJsonObject();
                    toCreate["Name"] = t.Name;
                    toCreate["Description"] = t.Description;
                    toCreate["WorkProduct"] = Ref.GetRelativeRef(createUserStoryResult.Reference);
                    // task
                    CreateResult createUserStoryTaskResult = restApi.Create("task", toCreate);
                }

            }

            // add tasks to UserStories


            ////Query for items
            //Request request = new Request("portfolioitem/feature");
            //request.Fetch = new List<string>() { "Name", "Description", "FormattedID" };
            //request.Query = new Query("_ref", Query.Operator.Equals, createResult.Reference);
            ////request.Query = new Query("Name", Query.Operator.Equals, "TEST_PortfolioItem_via_api");
            //QueryResult queryResult = restApi.Query(request);
            //foreach (var result in queryResult.Results)
            //{
            //    // get the RallyIDs for this item and store it
            //    RallyFeatureRef = result[2].Value;
            //    RallyFeatureRefObjectUUID = result[3].Value;
            //    RallyFeatureFormattedId = result[7].Value;
            //}

            //Delete the item
            //OperationResult deleteResult = restApi.Delete(createFeatureResult.Reference);

            return 123;

        }

        /// <summary>
        /// Create a Test UserStory
        /// </summary>
        /// <returns></returns>
        public int CreateTestUserStory()
        {
            //bool IsSuccess = false;

            // Initialize the REST API. You can specify a web service version if needed in the constructor.
            RallyRestApi restApi = GetRallyRestApi();

            //Create an item
            DynamicJsonObject toCreate = new DynamicJsonObject();
            toCreate["Name"] = "TEST_User story created from API by Beau";
            toCreate["Description"] = "Test description";

            // important to which this belongs
            toCreate["Workspace"] = "/workspace/" + WebConfigurationManager.AppSettings["RallyAPIWorkspaceID"];
            //toCreate["Project"] = "/project/32709392846d";
            //toCreate["Iteration"] = "/iteration/4459106059";

            // hierarchicalrequirement = UserStory
            CreateResult createResult = restApi.Create("hierarchicalrequirement", toCreate);

            //Get the item
            DynamicJsonObject item = restApi.GetByReference(createResult.Reference);

            //Query for items
            Request request = new Request("hierarchicalrequirement");
            request.Fetch = new List<string>() { "Name", "Description", "FormattedID" };
            request.Query = new Query("Name", Query.Operator.Equals, "TEST_User story created from API by Beau");
            QueryResult queryResult = restApi.Query(request);
            foreach (var result in queryResult.Results)
            {
                // get the RallyID for this item and store it

            }

            //Delete the item
            OperationResult deleteResult = restApi.Delete(createResult.Reference);

            return 123;

        }
        #endregion
    }
}
