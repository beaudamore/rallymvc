﻿using eBayData.DAL;
using eBayData.Models;
using RallyMVC.DAL;
using System;
namespace RallyMVC.Classes
{
    public class ErrorLogger: IDisposable
    {
        // repos for this controller - make sure to dispose
        private readonly IErrorRepository _errorRepository;
        // --

        public ErrorLogger() // initialize the repositories for this controller
        {
            this._errorRepository = new ErrorRepository(new RallyContext());
        }

        public void LogError(Exception ex, string userName)
        {
            var innerEx = string.Empty;
            if (ex.InnerException != null)
            {
                innerEx = ex.InnerException.Message;
            }
            var err = new Error()
            {
                ErrorDate = DateTime.Now,
                Message = ex.Message,
                InnerException = string.IsNullOrEmpty(innerEx) ? "" : innerEx,
                StackTrace = ex.StackTrace,
                User = userName
            };
            _errorRepository.InsertError(err);
            _errorRepository.Save();
        }
        public void Dispose()
        {
            _errorRepository.Dispose();
        }

    }
}
